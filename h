[33mcommit 65f2695a12866e6ed7fbd553aa1fc76d9714aa98[m[33m ([m[1;36mHEAD -> [m[1;32mmain[m[33m)[m
Author: frederic.bapst <frederic.bapst@hefr.ch>
Date:   Mon Dec 9 13:36:40 2024 +0100

    add s11
    
    (cherry picked from commit b1403669768a6b1998e2d1720dccb25d806c2439)

[33mcommit 630c49a3d35720f44fccc2e14c4892c356269fa8[m[33m ([m[1;31morigin/main[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Dec 5 10:51:10 2024 +0100

    finished s10 ex1 Treap

[33mcommit a31a4026504a6d89b5838c6f4884da6cf462b55e[m
Author: frederic.bapst <frederic.bapst@hefr.ch>
Date:   Mon Dec 2 10:01:37 2024 +0100

    add s10
    
    (cherry picked from commit 7910675dc5ffb817600ead093a597d003952b306)

[33mcommit b8c424cd3a21a7660f1eda3d7b35955a3d1d708c[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Dec 5 07:16:41 2024 +0100

    implemented S09

[33mcommit a71dc961786fdb14f357e42d148cd1c141abf285[m
Author: frederic.bapst <frederic.bapst@hefr.ch>
Date:   Wed Nov 20 09:52:37 2024 +0100

    add s09
    
    (cherry picked from commit 3defa09dd424cb5b721bcfd7952211d79eb16567)

[33mcommit 821bc5fcc4ff85a60973b6e88716d7dae9f668c7[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Nov 21 14:15:57 2024 +0100

    started S08

[33mcommit 6e80a306fbad455dbae188fa76fe7d31e7d6ec3a[m
Author: frederic.bapst <frederic.bapst@hefr.ch>
Date:   Mon Nov 11 14:46:49 2024 +0100

    add s08
    
    (cherry picked from commit 459b163e0a6949ff4d6b1e269bad218fd83f8ec0)

[33mcommit c9e93ae9e82adb268763ff312f760076d59945f9[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Mon Nov 11 17:05:05 2024 +0100

    implemented s12ex04

[33mcommit eb801015444c23075df6b13566bcfabbc7768cbb[m
Author: marvin egger <marvin.egger@edu.hefr.ch>
Date:   Sun Nov 10 14:39:31 2024 +0100

    finisged s07 ex1 and s12 ex3

[33mcommit ae5803e0fbef79a41e37b5ddc36c78a7e62f6e20[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Nov 7 13:04:13 2024 +0100

    implemented s07 DisjointSets, not tested and s12ex03

[33mcommit bb57f6f478fa5881fee97c93f6a19a44d2a1f40d[m
Author: frederic.bapst <frederic.bapst@hefr.ch>
Date:   Sun Nov 3 19:39:56 2024 +0100

    add s07
    
    (cherry picked from commit 59ec0258921942b408c197cf48763c1086297277)

[33mcommit ca8e196ec6b80b311f2f7eb5476c09a28bc12df4[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Mon Nov 4 17:34:14 2024 +0100

    implemented S12 ex 10

[33mcommit f2de1ccbc75406d8bba3e0602a7b56343d6ed0f8[m
Author: marvin egger <marvin.egger@edu.hefr.ch>
Date:   Mon Nov 4 07:10:20 2024 +0100

    added ex1

[33mcommit b6af9c7187cfeaeb543b531d69b70ee76e36249f[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Sat Nov 2 12:29:13 2024 +0100

    started s11

[33mcommit 167baf31cf879ed4ac94d56515a243a3259aba27[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Oct 31 09:31:10 2024 +0100

    started DiscreteSimulation

[33mcommit 968edfd5b89358e5c7ece7dd7f9c3eef715c7845[m
Merge: 1b732dd dbbd1f9
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Oct 31 07:19:43 2024 +0100

    Merge branch 'main' of gitlab.forge.hefr.ch:matteo.lupica/tp-algo-2-egg-lup

[33mcommit 1b732dd45c43295d1309c0689946e4cb0ab72229[m
Author: frederic.bapst <frederic.bapst@hefr.ch>
Date:   Sat Oct 26 09:14:08 2024 +0200

    add s06
    
    (cherry picked from commit c68a235c2ef051108ee77fd93ab5e9956bc753e4)

[33mcommit dbbd1f98ab2b5f9fdcc9c6cff1f0cfb82d5f4c9f[m
Author: marvin egger <marvin.egger@edu.hefr.ch>
Date:   Wed Oct 23 19:19:44 2024 +0200

    implemented s12 ex05 - AnagramDico

[33mcommit 73dc87cfd5d01ab8d09f90f7b2ed00bd5e4e949a[m
Author: marvin egger <marvin.egger@edu.hefr.ch>
Date:   Wed Oct 23 18:54:03 2024 +0200

    implemented s12 ex02 - rotation

[33mcommit 9e53e6907d159f02bf45e519e54b977e9e207468[m
Author: marvin egger <marvin.egger@edu.hefr.ch>
Date:   Wed Oct 23 17:31:37 2024 +0200

    implemented s12 ex0 (lifoQueue)

[33mcommit 68eb3e5c05f841abbb54c2b32f49b8da41b602c1[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Wed Oct 23 16:55:10 2024 +0200

    implemented nbOfKeysInRange

[33mcommit a6a34de4bd3fb511c3ca4218c193498121eb9f5a[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Wed Oct 23 13:27:52 2024 +0200

    implemented get method

[33mcommit c88c8fe9061657ff8f62307faa74714043f1c2b7[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Oct 17 08:21:06 2024 +0200

    added s05

[33mcommit a1fd109b504987fb654b4ee4e33502f60687264b[m
Author: frederic.bapst <frederic.bapst@hefr.ch>
Date:   Mon Oct 14 08:42:30 2024 +0200

    add s05
    
    (cherry picked from commit 4e483fb5b32fb378858522bc3acd2c36b0293c87)

[33mcommit 8dad73b2aba97dbfd2fa15fac0544a2350d3214c[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Oct 10 11:02:08 2024 +0200

    implemented optimalBST constructor

[33mcommit d205b3202508349130957f876361ffdd19ec6c71[m
Author: Matteo Lupica <matteo.lupica@edu.hefr.ch>
Date:   Thu Oct 10 10:45:19 2024 +0200

    Made left and right rotation method

[33mcommit 68837a67b780e110db12f5962f901424055a220f[m
Author: Matteo Lupica <matteo.lupica@edu.hefr.ch>
Date:   Thu Oct 10 10:18:14 2024 +0200

    BST class completed

[33mcommit 62415341874221db8e807af24691e59a750f863e[m
Merge: fc8db24 d0ae684
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Oct 10 07:49:08 2024 +0000

    Merge branch 'dev-marvin' into 'main'
    
    Dev marvin
    
    See merge request matteo.lupica/tp-algo-2-egg-lup!1

[33mcommit d0ae68435770bdb16f0c917c6317d7a512b1d876[m[33m ([m[1;32mdev-marvin[m[33m)[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Oct 10 09:45:19 2024 +0200

    started BTreeItr rotations

[33mcommit 732a42c71909f96084fad24faca31ff0d64513be[m
Merge: 1d9465a fc8db24
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Oct 10 09:39:44 2024 +0200

    Merge branch 'main' of gitlab.forge.hefr.ch:matteo.lupica/tp-algo-2-egg-lup

[33mcommit fc8db2430d018fe9fa043688b5026eb157be4add[m
Author: Matteo Lupica <matteo.lupica@edu.hefr.ch>
Date:   Thu Oct 10 09:39:29 2024 +0200

    adding S03 solution

[33mcommit 20d442d0ee014922d53fa28d68a8b51a3446f1e1[m
Author: Matteo Lupica <matteo.lupica@edu.hefr.ch>
Date:   Thu Oct 10 09:39:04 2024 +0200

    adding S02 solution

[33mcommit ce3188c5ecfb29918ffbd388d8d01e87595cd94d[m
Author: Matteo Lupica <matteo.lupica@edu.hefr.ch>
Date:   Thu Oct 10 09:38:27 2024 +0200

    adding S01 solution

[33mcommit 1d9465aaf6365f8bd014fc8a2d0eaac5a5e07243[m
Author: Marvin Egger <marvin.egger@edu.hefr.ch>
Date:   Thu Oct 10 09:38:25 2024 +0200

    started BTreeItr rotations

[33mcommit 480905a3643587f233b46833b93b14d74ff7f661[m
Author: frederic.bapst <frederic.bapst@hefr.ch>
Date:   Mon Oct 7 09:47:16 2024 +0200

    add s04

[33mcommit 262f1e5a536965ea875deb75a74b21d6d163387e[m
Author: frederic.bapst <frederic.bapst@hefr.ch>
Date:   Fri Sep 13 09:42:18 2024 +0200

    add tiny skeleton for s12.ex18b

[33mcommit ece4ae3547079e3180bf8c611d46f2c61bfbe586[m
Author: frederic.bapst <frederic.bapst@hefr.ch>
Date:   Mon Sep 9 18:31:00 2024 +0200

    initial commit
