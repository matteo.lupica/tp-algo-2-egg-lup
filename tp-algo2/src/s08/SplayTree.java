package s08;

import s12.ex03.BetterStack;

public class SplayTree<K extends Comparable<K>> {
  private BTree<K> tree = new BTree<>();
  private int crtSize = 0;  // currentSize

  public SplayTree() { super(); }

  public void add(K e) {
    if (contains(e)) return;  //This "splays" the tree!
    crtSize++;
    // Get iterator for the root of this tree
    BTreeItr<K> rootTi = tree.root();
    // Cut the tree, as we'll be adding e as the root
    BTree<K> oldTree = rootTi.cut();
    rootTi.insert(e);
    if (oldTree.isEmpty()) return; // there is nothing to paste
    BTreeItr<K> oldRootTi = oldTree.root();
    // if oldRoot > newRoot (e) : paste to the right, paste left subtree on the left
    if (oldRootTi.consult().compareTo(e) > 0) {
      if (oldRootTi.hasLeft())
        rootTi.left().paste(oldRootTi.left().cut());
      rootTi.right().paste(oldTree);
    } else { // Otherwise : do the opposite
      if (oldRootTi.hasRight())
        rootTi.right().paste(oldRootTi.right().cut());
      rootTi.left().paste(oldTree);
    }
  }

  public void remove(K e) {
    if (! contains(e)) return; // This "splays" the tree!
    crtSize--;
    if (tree.root().hasLeft() && tree.root().hasRight()) {
      BTree<K> oldRight = tree.root().right().cut();
      tree=tree.root().left().cut();
      BTreeItr<K> maxInLeft = tree.root().rightMost().up();
      BTreeItr<K> ti = splayToRoot(maxInLeft); // now tree has no right subtree!
      ti.right().paste(oldRight);
    } else {  // the tree has only one child
      if (tree.root().hasLeft()) tree = tree.root().left() .cut();
      else                       tree = tree.root().right().cut();
    }
  }

  public boolean contains(K e) {
    if (isEmpty()) return false;
    BTreeItr<K> ti = locate(e);
    boolean absent = ti.isBottom();
    if (absent) ti = ti.up();
    splayToRoot(ti);
    return !absent;
  }

  protected  BTreeItr<K> locate(K e) {
    BTreeItr<K> ti = tree.root();
    while(!ti.isBottom()) {
      K c = ti.consult();
      if (e.compareTo(c) == 0) break;
      if (e.compareTo(c) <  0) ti = ti.left();
      else                     ti = ti.right();
    }
    return ti;
  }

  public int size() { return crtSize; }

  public boolean isEmpty() { return size() == 0; }

  public K minElt() {
    BTreeItr<K> ti = tree.root().leftMost();
    if (ti.isRoot()) return null;
    return ti.up().consult();
  }

  public K maxElt() {
    BTreeItr<K> ti = tree.root().rightMost();
    if (ti.isRoot()) return null;
    return ti.up().consult();
  }

  @Override public String toString() {
    return "" + tree.toReadableString() + "SIZE:" + size();
  }
  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  // PRE:     ! ti.isBottom()
  // RETURNS: root position
  // WARNING: ti is no more valid afterwards
  private BTreeItr<K> splayToRoot(BTreeItr<K> ti) {
    while (!ti.isRoot()) {
    BTreeItr<K> parentIt = ti.up();

    if(parentIt.isRoot()) {
      ti = applyZig(ti);
    }else if(ti.isLeftArc() == parentIt.isLeftArc()){
      ti = applyZigZig(ti);

    }else {
      ti = applyZigZag(ti);
    }

    }
    return ti;
  }

  // PRE / RETURNS : Zig situation (see schemas)
  // WARNING: ti is no more valid afterwards
  private BTreeItr<K> applyZig(BTreeItr<K> ti) {
    boolean leftZig = ti.isLeftArc();
    ti = ti.up();
    if (leftZig) ti.rotateRight();
    else         ti.rotateLeft();
    return ti;
  }

  // PRE / RETURNS : ZigZig situation (see schemas)
  // WARNING: ti is no more valid
  private BTreeItr<K> applyZigZig(BTreeItr<K> ti) {
    ti = ti.up();
    ti = ti.up();

    if( ti.isLeftArc()){
      ti.rotateRight();
      ti.rotateRight();
    }else{
      ti.rotateLeft();
      ti.rotateLeft();
    }
    return ti;
  }

  // PRE / RETURNS : ZigZag situation (see schemas)
  // WARNING: ti is no more valid
  private BTreeItr<K> applyZigZag(BTreeItr<K> ti) {
  ti = applyZig(ti);
  return applyZig(ti);
  }
}
