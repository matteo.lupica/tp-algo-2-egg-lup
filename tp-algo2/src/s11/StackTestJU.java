package s11;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

public class StackTestJU {
  private IntStack s1, s2;

  @BeforeEach
  public void setUp() {
    s1 = new IntStack(10);
    s2 = new IntStack();
  }

  @Test
  public void testNewIsEmpty() {
    assertTrue(s1.isEmpty() && s2.isEmpty());
  }

  // covers
  @Test
  public void testPushThenPop() {
    s1.push(4);
    assertEquals(4, s1.pop());
  }

  @Test
  public void testPop(){
    s1 = new IntStack();
    s1.push(-1);
    assertEquals(s1.top(),-1);
    s1.push(0);
    assertEquals(s1.top(),0);

  }

  @Test
  public void testCheckOrder() {
    s1 = new IntStack();
    s1.push(1);
    s1.push(2);
    assertEquals(s1.isEmpty(),false);
    assertEquals(2, s1.pop());
    assertEquals(s1.isEmpty(), false);
    assertEquals(1, s1.pop());
    assertEquals(s1.isEmpty(), true);
  }

  @Test
  public void testCheckSize() {
    s1 = new IntStack(1);
    s1.push(1);
    s1.push(2);
    assertEquals(s1.isEmpty(),false);
    assertEquals(2, s1.pop());
    assertEquals(s1.isEmpty(), false);
    assertEquals(1, s1.pop());
    assertEquals(s1.isEmpty(), true);
  }
}
