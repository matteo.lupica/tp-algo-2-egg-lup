package s04;

import com.sun.source.tree.Tree;

public class BST<E extends Comparable<E>> {
  protected BTree<E> tree;
  protected int   crtSize;  // currentSize

  public BST() {
    tree = new BTree<>();
    crtSize = 0;
  }

  public BST(E[] tab) {  // PRE sorted, no duplicate
    tree = new BTree<>();
    crtSize = 0;
    for (E e: tab) {
      add(e);
    }
  }

  /** returns where e is, or where it should be inserted as a leaf */
  protected  BTreeItr<E> locate(E e) {
    BTreeItr<E> itr = new BTreeItr<>(tree);
    return locate(e,itr);
  }
  private BTreeItr<E> locate(E e, BTreeItr<E> itr){
    if(itr.isBottom() || e.compareTo(itr.consult()) == 0)
      return itr;
    else
        return e.compareTo(itr.consult()) < 0 ? locate(e,itr.left()) : locate(e,itr.right());
  }

  public void add(E e) {
    BTreeItr<E> treeItr = new BTreeItr<>(tree);
    if (!contains(e)) {
      BTreeItr<E> sub = locate(e, treeItr);
      if (sub.isBottom()) {
        sub.insert(e);
        crtSize++;
      }
    }
  }

  public void remove(E e) {
    if(contains(e)){
      crtSize--;
      BTreeItr<E> itr = locate(e, new BTreeItr<>(tree));
      remove(e,itr);
    }
  }
  private void remove(E e, BTreeItr<E> itr){
    if(itr.hasRight()){
      itr.rotateLeft();
      remove(e,itr.left());
      return;
    }
    if(itr.hasLeft()){
      BTree<E> leftSide = itr.left().cut();
      itr.paste(leftSide);
    }else{
      itr.cut();
    }
  }

  public boolean contains(E e) {
    BTreeItr<E> ti = locate(e);
    return ! ti.isBottom();
  }

  public int size() {
    return crtSize;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public E minElt() {
    BTreeItr<E> itr = new BTreeItr<>(tree);
    while (!itr.isBottom())
      itr = itr.left();
    return itr.up().consult();
  }

  public E maxElt() {
    BTreeItr<E> itr = new BTreeItr<>(tree);
    while (!itr.isBottom())
      itr = itr.right();
    return itr.up().consult();
  }

  @Override public String toString() {
    return "" + tree;
  }
  
  public String toReadableString() {
    String s = tree.toReadableString();
    s += "size==" + crtSize + "\n";
    return s;
  }
  
  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  //PRE: array without duplicates
  private BTree<E> optimalBST(E[] sorted, int left, int right) {

    BTree<E> r = new BTree<>();

    if(left > right)
      return r;

    int mid = (left + right)/2;
    BTreeItr<E> ri = r.root();
    ri.update(sorted[mid]);

    ri.paste( optimalBST(sorted,left,mid-1));
    ri.paste(optimalBST(sorted,mid+1,right));

    return r;
  }
}
