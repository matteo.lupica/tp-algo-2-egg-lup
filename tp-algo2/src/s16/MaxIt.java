package s16;
import java.util.Random;
import java.io.*;
import java.util.StringTokenizer;
import java.util.Stack;

public class MaxIt {
  private static Random rnd = new Random(12);
  //======================================================================
  record Move(int row, int col, boolean isPlayerA) {}
  
  record MinMaxResult(Move chosenMove, int expectedOutcome) {}
  
  interface MaxitStrategy {
    Move decision(Board board, boolean isPlayerA);
  }
  //======================================================================
  static class Board {
    protected int    [][] grid;
    protected boolean[][] isUsed;
    protected int pointsOfA = 0;
    protected int pointsOfB = 0;
    protected int currentRow = 0;
    protected int currentCol = 0;
    protected int usedCells = 0;
    private final Stack<Move> moves = new Stack<>();

    public Board(int dimension) {
      grid   = new int    [dimension][dimension];
      isUsed = new boolean[dimension][dimension];
    }

    public int dimension() { return grid.length; }

    public boolean isValidMove(Move m) {
      if (m.col<0 || m.col>grid.length-1)        return false;
      if (m.row<0 || m.row>grid[m.col].length-1) return false;
      if (isUsed[m.col][m.row])                  return false;
      return (m.col == currentCol)  || (m.row == currentRow);
    }

    public Move[] possibleMoves(boolean isPlayerA) {
      Move[] res;
      int i=0; int j=0; int n=0;
      Stack<Move> s = new Stack<>();
      Move m;

      for (i=0; i<grid.length; i++) {
        m = new Move(i, currentCol, isPlayerA);
        if (isValidMove(m)) {
          n++;
          s.push(m);
        }
      }
      for (j=0; j<grid[0].length; j++) {
        m = new Move(currentRow, j, isPlayerA);
        if (isValidMove(m)) {
          n++;
          s.push(m);
        }
      }
      res = new Move[n];
      while (!s.empty()) {
        res[--n] = s.pop();
      }
      return res;
    }

    public void play(Move m) {
      sanityCheckBeforePlaying(m);
      moves.push(m);
      usedCells ++;
      isUsed[m.col][m.row] = true;
      if (m.isPlayerA)
        pointsOfA += grid[m.col][m.row];
      else
        pointsOfB += grid[m.col][m.row];
      currentCol = m.col;
      currentRow = m.row;
    }

    private void sanityCheckBeforePlaying(Move m) {
      if(m.row != currentRow && m.col != currentCol)
        throw new IllegalArgumentException("you _must_ choose a cell in the current row or column!");
      if(isUsed[m.col][m.row])
        throw new IllegalArgumentException("this cell has already been played!!");
    }

  // PRE: at least one move was played
  public void undo() {
      // TODO - A COMPLETER
      assert (!moves.empty());
      assert (usedCells > 0);

      Move lastMove = moves.pop();
      //if player A did last move
      usedCells --;
      //set cell to be able to be used again
      isUsed[lastMove.col][lastMove.row] = false;
      // reduce score of last player
      if(lastMove.isPlayerA()){
        pointsOfA -= grid[lastMove.col][lastMove.row];
      }else{
        pointsOfB -= grid[lastMove.col][lastMove.row];
      }

      // set current col and row to the values of the moves before last
      Move lastValidMove = moves.peek();
      currentCol = lastValidMove.col;
      currentRow = lastValidMove.row;
    }

    /** The "measure" according to the general MiniMax model */
    public int score() {
      return pointsOfA - pointsOfB;
    }

    public boolean isGameOver() {
      return possibleMoves(false).length == 0;
    }

    public String toString() {
      String res = "";
      for (int i=0; i<grid.length; i++) {
        for (int j=0; j<grid[i].length; j++) {
          if (isUsed[i][j]) {
            if (i==currentCol && j==currentRow) {
              res += " !!";
            } else {
              res += " --";
            }
          } else {
            res += " " + grid[i][j];
          }
        }
        res += "\n";
      }
      res += "          A: " + pointsOfA+",     B:" + pointsOfB ;
      res += "\n";
      return res;
    }

    public static Board rndBoard(int dim) {
      Board b = new Board(dim);
      for (int i=0; i<dim; i++)
        for (int j=0; j<dim; j++) {
          b.grid[i][j] = rnd.nextInt(90) + 10;
        }
      return b;
    }

    public static Board rndBoard(int dim, long seed) {
      rnd = new Random(seed);
      return rndBoard(dim);
    }
  }
  //======================================================================

  public static Move readMove(Board b, boolean isPlayerA) {
    Move m = null;
    try {
      do {
        m = enterMove(isPlayerA);
      } while (! b.isValidMove(m));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return m;
  }

  private static Move enterMove(boolean isPlayerA) throws IOException {
    BufferedReader is = new BufferedReader(new InputStreamReader(System.in));
    System.out.print(isPlayerA?"PlayerA":"PlayerB");
    System.out.print(", enter row and column: ");
    String s = is.readLine();
    StringTokenizer st = new StringTokenizer(s);
    int row = Integer.parseInt(st.nextToken());
    int col = Integer.parseInt(st.nextToken());
    return new Move(row, col, isPlayerA);
  }

  public static Move rndMove(Board board, boolean isPlayerA) {
    Move[] mt = board.possibleMoves(isPlayerA);
    int i = rnd.nextInt(mt.length);
    return mt[i];
  }

  /** @return the expected score, as well as the move that
   *   has been chosen by the MiniMax algorithm (null when game is over),
   *   limited to a computation depth of "levels". */
  static MinMaxResult expectedScore(Board board,  boolean isPlayerA,
                                    int   levels) {
    // "terminal" configuration
    if (board.isGameOver())
      return new MinMaxResult(null, board.score());

    Move[] mt = board.possibleMoves(isPlayerA);
    // "quasi-terminal" configuration

    if (levels==0) {
      // mt[0] is just to announce something (probably the caller won't use
      return new MinMaxResult(mt[0], board.score());   // it anyway!)
    }
    Move idealMove = mt[0];
    int idealScore = isPlayerA ? Integer.MIN_VALUE : Integer.MAX_VALUE;

    for (int i = 0; i < mt.length; i++) {
      board.play(mt[i]);
      MinMaxResult m = expectedScore(board, !isPlayerA, levels - 1);
      board.undo();
      int expOut = m.expectedOutcome;
      if (isPlayerA) { //max
        if (expOut > idealScore) {
          idealMove = mt[i];
          idealScore = expOut;
        }
        } else if (expOut < idealScore) { // min
          idealScore = expOut;
          idealMove = mt[i];
        }
    }
return new MinMaxResult(idealMove,idealScore);
  }
  public static Move greedyMove(Board board, boolean isPlayerA) {
    Move[] mt = board.possibleMoves(isPlayerA);
    Move bestMove=mt[0];
    int best=board.grid[bestMove.col][bestMove.row];
    for (Move m: mt) {
      int x = board.grid[m.col][m.row];
      if (x > best) {
        best = x;
        bestMove = m;
      }
    }
    return bestMove;
  }

  static Move bestMove(Board board, boolean isPlayerA, int levels) {
    MinMaxResult outcome = expectedScore(board, isPlayerA, levels);
    return outcome.chosenMove;
  }

  static Move bestMoveAlphaBeta(Board board, boolean isPlayerA, int levels) {
    return expectedScore(board, isPlayerA, levels, 
                         Integer.MIN_VALUE, Integer.MAX_VALUE).chosenMove; 
}

  static MinMaxResult expectedScore(Board board, boolean isPlayerA, 
                                           int levels, int alpha, int beta) {
    // "terminal" configuration
    if (board.isGameOver())
      return new MinMaxResult(null, board.score());

    Move[] mt = board.possibleMoves(isPlayerA);
    // "quasi-terminal" configuration

    if (levels==0) {
      // mt[0] is just to announce something (probably the caller won't use
      return new MinMaxResult(mt[0], board.score());   // it anyway!)
    }
    Move idealMove = mt[0];
    int idealScore = isPlayerA ? Integer.MIN_VALUE : Integer.MAX_VALUE;

    for (int i = 0; i < mt.length; i++) {
      board.play(mt[i]);
      MinMaxResult m = expectedScore(board, !isPlayerA, levels - 1);
      board.undo();
      int expOut = m.expectedOutcome;
      if (isPlayerA) { //max
        if (expOut > idealScore) {
          idealMove = mt[i];
          idealScore = expOut;
          alpha = Math.max(alpha, idealScore);

        }
      } else if (expOut < idealScore) { // min
        idealScore = expOut;
        idealMove = mt[i];
        beta = Math.min(beta, idealScore);

      }

      if(beta <= alpha)
        break;

    }

    return new MinMaxResult(idealMove,idealScore);
  }
  
  // ------------------------------------------------------------
  static void playGame(int boardSize,  MaxitStrategy a, MaxitStrategy b, String msg) {
    long fixedRndSeed = 12;
    Board board = Board.rndBoard(boardSize, fixedRndSeed);
    boolean isPlayerA = true;
    long t1 = System.nanoTime();
    while (!board.isGameOver()) {
      // System.out.println(board); // uncomment to follow step-by-step…
      Move move = (isPlayerA ? a : b).decision(board, isPlayerA);
      board.play(move);
      isPlayerA = ! isPlayerA;
    }
    long t2 = System.nanoTime();
    System.out.print(board);
    System.out.println("Game over - " + msg);
    System.out.println("total time: " + (t2-t1)/1000/1000 + " [ms]\n");
  }
  
  public static void main(String[] args) {
    int depth = 5; // recursion depth (nLevels)
    int n = 6; // boardSize
    MaxitStrategy rnd =        (x,y) -> rndMove(x, y);
    MaxitStrategy human =      (x,y) -> readMove(x, y);
    MaxitStrategy greedy =     (x,y) -> greedyMove(x, y);
    MaxitStrategy minMax =     (x,y) -> bestMove(x, y, depth);
    MaxitStrategy minMaxPoor = (x,y) -> bestMove(x, y, depth - 2);
    MaxitStrategy minMaxRich = (x,y) -> bestMove(x, y, depth + 2);
    MaxitStrategy alpha =      (x,y) -> bestMoveAlphaBeta(x, y, depth);
    MaxitStrategy alphaPoor =  (x,y) -> bestMoveAlphaBeta(x, y, depth - 2);
    MaxitStrategy alphaRich =  (x,y) -> bestMoveAlphaBeta(x, y, depth + 2);

    playGame(n, rnd, minMax, "rnd-against-minMax");
    playGame(n, rnd, alpha, "rnd-against-AlphaBeta");


    // Now it's your turn: conduct some other relevant experiments
    // - to validate the correctness of bestMove()
    // - to validate the correctness of bestMoveAlphaBeta()
    // - to get some hints on the speedup of alphaBeta pruning

    // compare poor and rich alpha beta
    playGame(n, rnd, alphaPoor, "rnd-against-AlphaBeta-Poor");
    playGame(n, rnd, alphaRich, "rnd-against-AlphaBeta-Rich");

    // compare poor and rich minMAx
    playGame(n, rnd, minMaxPoor, "rnd-against-minMaxPoor-Poor");
    playGame(n, rnd, minMaxRich, "rnd-against-minMaxRich-Rich");

    // compare against greedy
    playGame(n, greedy, alpha, "greedy-against-AlphaBeta");
    playGame(n, greedy, minMax, "greedy-against-minMax");

    // compare poor and rich minMAx
    playGame(n, greedy, minMaxPoor, "greedy-against-minMaxPoor-Poor");
    playGame(n, greedy, minMaxRich, "greedy-against-minMaxRich-Rich");


  }
  // ------------------------------------------------------------
  // No test program is provided. Normally the MinMax strategy (B)  
  // should win against Random (A). Here is what I obtained with
  // level=5 and boardSize=6   ------>     A: 688,     B:1052
}
