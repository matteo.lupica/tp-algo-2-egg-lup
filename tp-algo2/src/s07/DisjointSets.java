package s07;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

// ------------------------------------------------------------
public class DisjointSets {

    //if the parent is null elt is a root
    public static final int NULL_INDEX = -1;
    private int nbSets; // amount of sets

    //if [i] >= 0 value represents parent
    //if[i] < 0, elt is root and value represents nb of children *(-1)
    private int[] parents;
    private int[] mins;

    // PRE: nbOfElements >= 0
    public DisjointSets(int nbOfElements) {
        //at the start each elemement is its own set
        parents = new int[nbOfElements];
        mins = new int[nbOfElements];
        nbSets = nbOfElements;

        for (int i = 0; i < nbOfElements; i++) {
            parents[i] = NULL_INDEX;
            mins[i] = i;
        }
    }

    /**
     * determines whether i and j are in the same group.
     * PRE: 0 <= i,j < nbOfElements()
     */
    public boolean isInSame(int i, int j) {
        return findRoot(i) == findRoot(j);
    }

    /**
     * merges the respective groups containing i and j
     * PRE: 0 <= i,j < nbOfElements()
     */
    public void union(int i, int j) {
        int rooti = findRoot(i);
        int rootj = findRoot(j);

        if (rooti == rootj) return;

        //values are size + (-1) so the smaller one contains more elements
        int bigger = (parents[rooti] < parents[rootj]) ? rooti : rootj;
        int smaller = (rooti == bigger) ? rootj : rooti;

        // Merge the smaller set into the larger set
        parents[bigger] += parents[smaller];
        mins[bigger] = Math.min(mins[bigger], mins[smaller]);
        parents[smaller] = bigger;

        nbSets--;

    }

    private int findRoot(int i) {
        if (parents[i] < 0)
            return i;

        // update parents while parsing
        parents[i] = findRoot(parents[i]);
        return parents[i];
    }

    public int nbOfElements() {  // as given in the constructor
        return parents.length;
    }

    /**
     * @return the smallest value in the same group as i
     * PRE: 0 <= i < nbOfElements()
     */

    public int minInSame(int i) {
        return mins[findRoot(i)];
    }

    /**
     * @return true if all elements are in the same group
     */
    public boolean isUnique() {
        return nbSets == 1;
    }

    /**
     * String format like this: "{0}{3,2,4}{1,5}"
     */
    @Override
    public String toString() {
        String res = "";
        int n = nbOfElements();
        boolean[] visited = new boolean[n];  // Track visited elements

        for (int i = 0; i < n; i++) {
            // if(i is not a root) continue;  // TODO
            if (parents[i] >= 0) {
                continue;
            }
            res += "{";
            Queue<Integer> q = new LinkedList<>();  // for breadth-first traversal
            q.add(i);
            visited[i] = true;

            while (!q.isEmpty()) {
                int v = q.remove();
                res += v;

                for (int j = 0; j < n; j++) {

                    if (parents[j] == v && !visited[j]) {
                        q.add(j);
                        visited[j] = true;
                        res += ",";
                    }
                }
            }
            res += "}";
        }
        return res;
    }

    /**
     * whether both DisjointSets represent the same logical set of groups
     */
    @Override
    public boolean equals(Object otherDisjSets) {
        if (this == otherDisjSets) return true;

        if (!(otherDisjSets instanceof DisjointSets)) return false;

        if (((DisjointSets) otherDisjSets).nbOfElements() != this.nbOfElements()) return false;

        // root of each element could change, but the min element of a set should always be the same
        for (int i = 0; i < nbOfElements(); i++) {
            if (minInSame(i) != ((DisjointSets) otherDisjSets).minInSame(i)) return false;
        }
        return true; // All roots matched, the sets are equal
    }

    public static void main(String[] args) {
        DisjointSets set = new DisjointSets(9);
        System.out.println(set.toString());
        set.union(0, 5);
        System.out.println(set.toString());
        set.union(8, 6);
        System.out.println(set.toString());
    }
}
