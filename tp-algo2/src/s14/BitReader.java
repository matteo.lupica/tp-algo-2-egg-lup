package s14;

import java.io.*;

// Reads any file one bit at a time (most significant bit first)
public class BitReader {

  private static final int MAX = 7;


  private final FileInputStream fis;
  // TODO - A COMPLETER

  private byte buffer;
  private int index;
  private boolean over;
  // private...

  public BitReader(String filename) throws IOException {
    fis = new FileInputStream(filename);
    index = MAX;
    buffer = 0;
    over = false;

    // read first byte and check if file empty
    readAndCheck();

  }

  private void readAndCheck() throws IOException {
    int v = fis.read();

    if(v == -1){
      over = true;
      return;
    }

    buffer = (byte) v;
  }

  public void close() throws IOException {
    fis.close();
  }


  //PRE: still bytes to read, over = false
  public boolean next() throws IOException {

    boolean b = getBit(index--);

    if(index == -1){
      readAndCheck();
      index = MAX;
    }

    return b;
  }

  private boolean getBit(int index){

     int msk = 1 << (index);

    if((buffer&msk) == 0)
      return false;

    return true;
  }

  public boolean isOver() {
    return over;
  }
  
  //-------------------------------------------
  // Tiny demo...
  public static void main(String [] args) {
    String filename = "a.txt";
    try {
      BitReader b = new BitReader(filename);
      int i=0;
      while(!b.isOver()) {
        System.out.print(b.next() ? "1" : "0");
        i++;
        if (i%8  == 0) System.out.print(" ");
        if (i%80 == 0) System.out.println("");
      }
      b.close();
    } catch (IOException e) {
      throw new RuntimeException("" + e);
    }
  }
}
