package s14;

import java.io.*;

// Produces a file one bit at a time, (most significant bit first)  
// filling the last byte with zeros if necessary
public class BitWriter {

  private final static int MAX = 7;

  private final FileOutputStream fos;
  // TODO - A COMPLETER
  private byte buffer;
  private int index;
  boolean buffEmpty;
  // private...

  public BitWriter(String filename) throws IOException {
    fos = new FileOutputStream(filename);
    buffer = 0;
    index = MAX;
    buffEmpty = true;
  }

  public void close() throws IOException {
    if(!buffEmpty)
      fos.write(buffer);

    fos.close();
  }

  public void put(boolean b) throws IOException {
      buffEmpty = false;
      setBit(index--,b);

      if(index < 0){
        fos.write(buffer);
        buffer = 0;
        index = MAX;
        buffEmpty = true;
      }
  }


  private void setBit(int index,boolean b){

    int msk = 1 << (index);
    //set bit
    if(b){
      buffer |= msk;
    }else{
      //clear bit
      buffer &= ~msk;
    }
  }
  
  //-------------------------------------------------------
  // Tiny demo...
  public static void main(String [] args) {
    String filename = "a.txt";
    String bytes = "01000001 01000010 010001";
    //                 A        B        D (01000100)
    try {
      BitWriter b = new BitWriter(filename);
      for(int i=0; i<bytes.length(); i++) {
        if (bytes.charAt(i) == '1') b.put(true);
        if (bytes.charAt(i) == '0') b.put(false);
      }
      b.close();
    } catch (IOException e) {
      throw new RuntimeException("" + e);
    }
  }
}
