package tsp;

//======================================================================
public class TSP41 implements TSP {
  public void salesman(TSPPoint[] t, int[] path) {


    int n = t.length;
    if (n == 0) return;

    // Copy t to avoid modifying the original array
    double[] tx = new double[n];
    double[] ty = new double[n];
    for (int i = 0; i < n; i++) {
      tx[i] = t[i].x;
      ty[i] = t[i].y;
    }

    // fill original indexes array
    int[] ogIndex = new int[n];
    for (int i = 0; i < n; i++) {
      ogIndex[i] = i;
    }

    // Start from the last city
    int thisPt = n - 1;
    int nbVisited = 0;

    // Move starting city to index 0
    swap(tx,ty, 0, thisPt);
    swap(ogIndex, 0, thisPt);

    path[0] = ogIndex[0];  // Store the original index of the start city
    nbVisited++;

    for (int i = 1; i < n; i++) {
      int closestPt = nbVisited;
      double shortestDist = Double.MAX_VALUE;

      // Find the closest unvisited city
      for (int j = nbVisited; j < n; j++) {
        double dx = tx[nbVisited - 1] - tx[j];
        double dy = ty[nbVisited - 1] - ty[j];
        double dist = dx * dx + dy * dy;

        if (dist < shortestDist) {
          shortestDist = dist;
          closestPt = j;
        }
      }

      // Swap closest city to position nbVisited
      swap(tx,ty, nbVisited, closestPt);
      swap(ogIndex, nbVisited, closestPt);

      path[i] = ogIndex[nbVisited];  // Store the original index
      nbVisited++;
    }
  }

  // Swap function for TSPPoint array
  private void swap(double[] x, double[] y, int i, int j) {
    double tempx = x[i];
    x[i] = x[j];
    x[j] = tempx;

    double tempy = y[i];
    y[i] = y[j];
    y[j] = tempy;
  }


  // Swap function for integer index array
  private void swap(int[] arr, int i, int j) {
    int temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }
}
