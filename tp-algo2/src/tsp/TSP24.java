package tsp;
//======================================================================
public class TSP24 implements TSP {
  public void salesman(TSPPoint[] t, int[] path) {
    int n = t.length;
    if (n == 0) return;
    boolean[] visited = new boolean[n];
    int thisPt = n - 1;  // Start from the last city
    visited[thisPt] = true;
    path[0] = thisPt;
    double[] tx = new double[n];
    double[] ty = new double[n];
    for (int i = 0; i < n; i++) {
      tx[i] = t[i].x;
      ty[i] = t[i].y;
    }

    for (int i = 1; i < n; i++) {
      int closestPt = -1;
      double shortestDistSq = Double.MAX_VALUE;
      TSPPoint currentPoint = t[thisPt];
      double cx = currentPoint.x;
      double cy = currentPoint.y;
      for (int j = 0; j < n; j++) {
        if (!visited[j]){
          double dx = cx - tx[j];
          double dy = cy - ty[j];
          if ((dx * dx + dy * dy) < shortestDistSq) {
            shortestDistSq = dx * dx + dy * dy;
            closestPt = j;
          }
        }
      }

      if (closestPt != -1) {
        path[i] = closestPt;
        visited[closestPt] = true;
        thisPt = closestPt;
      }
    }
  }
  //----------------------------------
  static private double sqr(double a) {
    return a*a;
  }
  //---------------------------------
  static private double distance(TSPPoint p1, TSPPoint p2) {
    //2.0 avoid method call

    //2.1 remove 2 times evaluations
    double pxVal = (p1.x - p2.x);
    double pyVal = (p1.y - p2.y);

    return Math.sqrt(pxVal*pxVal + (pyVal)*(pyVal));


  }
}