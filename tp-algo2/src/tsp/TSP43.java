package tsp;

//======================================================================
public class TSP43 implements TSP {
  public void salesman(TSPPoint[] t, int[] path) {


    int n = t.length;
    if (n == 0) return;

    // Copy t to avoid modifying the original array
    double[] tx = new double[n];
    double[] ty = new double[n];
    for (int i = 0; i < n; i++) {
      tx[i] = t[i].x;
      ty[i] = t[i].y;
    }

    int[] ogIndex = new int[n];

    for (int i = 0; i < n; i++) {
      ogIndex[i] = i;
    }

    // Start from the last city
    int thisPt = n - 1;
    int nbVisited = 0;

    // Move starting city to index 0
    double tempx = tx[0];
    tx[0] = tx[thisPt];
    tx[thisPt] = tempx;

    double tempy = ty[0];
    ty[0] = ty[thisPt];
    ty[thisPt] = tempy;
    // swap index
    int temp = ogIndex[0];
    ogIndex[0] = ogIndex[thisPt];
    ogIndex[thisPt] = temp;

    path[0] = ogIndex[0];  // Store the original index of the start city
    nbVisited++;

    for (int i = 1; i < n; i++) {
      int closestPt = nbVisited;
      double shortestDist = Double.MAX_VALUE;

      // Find the closest unvisited city
      for (int j = nbVisited; j < n; j++) {
        double dx = tx[nbVisited - 1] - tx[j];
        double dy = ty[nbVisited - 1] - ty[j];
        double dist = dx * dx + dy * dy;

        if (dist < shortestDist) {
          shortestDist = dist;
          closestPt = j;
        }
      }

      // Swap closest city to position nbVisited
      tempx = tx[nbVisited];
      tx[nbVisited] = tx[closestPt];
      tx[closestPt] = tempx;

      tempy = ty[nbVisited];
      ty[nbVisited] = ty[closestPt];
      ty[closestPt] = tempy;
      // swap index
      temp = ogIndex[nbVisited];
      ogIndex[nbVisited] = ogIndex[closestPt];
      ogIndex[closestPt] = temp;


      path[i] = ogIndex[nbVisited];  // Store the original index
      nbVisited++;
    }
  }
}
