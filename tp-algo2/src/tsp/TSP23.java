package tsp;
//======================================================================
public class TSP23 implements TSP {
  public void salesman(TSPPoint[] t, int[] path) {
    int i, j;
    int n = t.length;
    boolean[] visited = new boolean[n];
    int thisPt, closestPt = 0;
    double shortestDist;

    thisPt = n-1;
    if (thisPt < 0) return;
    visited[thisPt] = true;
    path[0] = n-1;  // chose the starting city
    for(i=1; i<n; i++) {
      shortestDist = Double.MAX_VALUE;
      for(j=0; j<n; j++) {
        if (visited[j])
          continue;
        //11 -> remove 2times method call
        //22 -> remove method call

        double pxVal = (t[thisPt].x - t[j].x);
        double pyVal = (t[thisPt].y - t[j].y);

        if (Math.sqrt(pxVal*pxVal + (pyVal)*(pyVal)) < shortestDist ) {

            shortestDist = Math.sqrt(pxVal*pxVal + (pyVal)*(pyVal));
          closestPt = j;
        }
      }
      path[i] = closestPt;
      visited[closestPt] = true;
      thisPt = closestPt;
    }
  }
  //----------------------------------
  static private double sqr(double a) {
    return a*a;
  }
  //---------------------------------
  static private double distance(TSPPoint p1, TSPPoint p2) {
    //2.0 avoid method call

    //2.1 remove 2 times evaluations
    double pxVal = (p1.x - p2.x);
    double pyVal = (p1.y - p2.y);

    return Math.sqrt(pxVal*pxVal + (pyVal)*(pyVal));


  }
}
