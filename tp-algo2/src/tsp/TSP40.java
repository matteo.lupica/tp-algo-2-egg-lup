package tsp;

//======================================================================
public class TSP40 implements TSP {
  public void salesman(TSPPoint[] t, int[] path) {
    int n = t.length;
    if (n == 0) return;

    // Copy t to avoid modifying the original array
    TSPPoint[] t2 = t.clone();
    int[] ogIndex = new int[n];

    for (int i = 0; i < n; i++) {
      ogIndex[i] = i;
    }

    // Start from the last city
    int thisPt = n - 1;
    int nbVisited = 0;

    // Move starting city to index 0
    swap(t2, 0, thisPt);
    swap(ogIndex, 0, thisPt);

    path[0] = ogIndex[0];  // Store the original index of the start city
    nbVisited++;

    for (int i = 1; i < n; i++) {
      int closestPt = nbVisited;
      double shortestDist = Double.MAX_VALUE;

      // Find the closest unvisited city
      for (int j = nbVisited; j < n; j++) {
        double dx = t2[nbVisited - 1].x - t2[j].x;
        double dy = t2[nbVisited - 1].y - t2[j].y;
        double dist = dx * dx + dy * dy;

        if (dist < shortestDist) {
          shortestDist = dist;
          closestPt = j;
        }
      }

      // Swap closest city to position nbVisited
      swap(t2, nbVisited, closestPt);
      swap(ogIndex, nbVisited, closestPt);

      path[i] = ogIndex[nbVisited];  // Store the original index
      nbVisited++;
    }
  }

  // Swap function for TSPPoint array
  private void swap(TSPPoint[] arr, int i, int j) {
    TSPPoint temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }

  // Swap function for integer index array
  private void swap(int[] arr, int i, int j) {
    int temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
  }
}
