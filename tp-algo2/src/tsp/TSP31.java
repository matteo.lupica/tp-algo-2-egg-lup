package tsp;
//======================================================================
public class TSP31 implements TSP {
  public void salesman(TSPPoint[] t, int[] path) {
    int i, j;
    //3.1 remove extra var
  //  int n = t.length;
    boolean[] visited = new boolean[ t.length];
    int thisPt, closestPt = 0;
    double shortestDist;

    thisPt =  t.length-1;
    if (thisPt < 0) return;
    visited[thisPt] = true;
    path[0] =  t.length-1;  // chose the starting city

    for(i=1; i< t.length; i++) {
      shortestDist = Double.MAX_VALUE;
      for(j=0; j< t.length; j++) {
        if (visited[j])
          continue;
        //11 -> remove 2times method call
        //22 -> remove method call - flatten

        //23 ->
        double pxVal = (t[thisPt].x - t[j].x);
        double pyVal = (t[thisPt].y - t[j].y);
        double distance = Math.sqrt(pxVal*pxVal + (pyVal)*(pyVal));

        if (distance < shortestDist ) {
          shortestDist = distance;
          closestPt = j;
        }
      }
      path[i] = closestPt;
      visited[closestPt] = true;
      thisPt = closestPt;
    }
  }
  //----------------------------------
  static private double sqr(double a) {
    return a*a;
  }
  //---------------------------------
  static private double distance(TSPPoint p1, TSPPoint p2) {
    //2.0 avoid method call

    //2.1 remove 2 times evaluations
    double pxVal = (p1.x - p2.x);
    double pyVal = (p1.y - p2.y);

    return Math.sqrt(pxVal*pxVal + (pyVal)*(pyVal));


  }
}
