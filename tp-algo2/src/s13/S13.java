
package s13;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.*;

public class S13 {
  
  // ------ Ex. 1 -----------

  public static void printSomePowersOfTwo() {
             IntStream.rangeClosed(1,10)
            .map(n -> (int)(Math.pow(n,2)))
            .forEach(System.out::println);

  }

    // ------ Ex. 2 -----------
  public static String macAddress(int[] t) {

    String addr = Arrays.stream(t)
            .mapToObj(i -> String.format("%02X",i))
            .collect(Collectors.joining(":"));
   return addr;
  }
  
  // ------ Ex. 3 -----------

  public static long[] divisors(long n) {
   return LongStream.range(1,n)
            .filter(d -> (n%d)==0)
            .toArray();
  }

  // ------ Ex. 4a -----------
  public static long sumOfDivisors(long n) {
    return Arrays.stream(divisors(n)).sum();
  }
  
  
  // ------ Ex. 4b -----------
  public static long[] perfectNumbers(long max) {
    return LongStream.range(0,max)
            .map(S13::sumOfDivisors)
            .toArray();
  }
  
  // ------ Ex. 5 -----------
  public static void printMagicWord() throws IOException {
    System.out.println(FileSystems.getDefault());
    Path path = FileSystems.getDefault().getPath("wordlist.txt");
    Files.lines(path)
      .filter(x -> x.length() == 11)
      .filter(x -> x.charAt(2) == 't')
      .filter(x -> x.charAt(4) == 'l')
      .filter(x -> x.chars().distinct().count() == 6)
      .forEach(System.out::println);
   }
  
  public static boolean isPalindrome(String str) {
    return str.equals(new StringBuilder(str).reverse().toString());
  }

  public static void printPalindromes() throws IOException {
    Path path = FileSystems.getDefault().getPath("wordlist.txt");
    String s = Files.lines(path)
            .filter(word -> isPalindrome(word))
            .collect(Collectors.joining(" ; "));
    System.out.println(s);
  }

  // ------ Ex. 6 -----------
  public static void averages() {
    double[][] results = {
        { 9, 10,  8,  5,  9},
        { 5,  9,  9,  8,  8},
        { 4,  8, 10,  9,  5},
        { 8, 10,  8, 10,  7},
        { 8,  9,  7, 10,  6},
    };

    double[] grades = new double[results.length];
    for (int i = 0; i < results.length; i++) {
        double sum = 0;
        for (int j = 0; j < results[i].length; j++) {
            sum += results[i][j];
        }
        grades[i] = 1.0 + sum / 10.0;
    }
    double sum = 0;
    for (int i = 0; i < grades.length; i++) {
        sum += grades[i];
    }
    double average = sum / grades.length;

    System.out.printf("Grades : %s%n", Arrays.toString(grades));
    System.out.printf("Average: %.2f%n", average);
  }

  public static void averagesWithStreams() {
    double[][] results = {
        { 9, 10,  8,  5,  9},
        { 5,  9,  9,  8,  8},
        { 4,  8, 10,  9,  5},
        { 8, 10,  8, 10,  7},
        { 8,  9,  7, 10,  6},
    };
    
    double[] grades = Arrays.stream(results)
            .mapToDouble(a -> Arrays.stream(a).sum() / 10 + 1.0)
            .toArray();

    //double average = Arrays.stream(grades).map(d -> d /grades.length).sum();
    double average = Arrays.stream(grades).average().orElse(0);

    System.out.printf("Grades : %s%n", Arrays.toString(grades));
    System.out.printf("Average: %.2f%n", average);
  }

  // ------ Ex. 8 -----------

  static DoubleStream sampling(double from, double to, int nSubSamples) {
   // DoubleStream
    return IntStream.range(0,nSubSamples)
     .mapToDouble(i -> from + i * (to -from) / (nSubSamples));
  }
  
  public static void ex8() {    

    // System.out.println(... product of:  1.2, 3.4, 5.6
    // Product of: 1.2, 3.4, 5.6
    double product = Stream.of(1.2, 3.4, 5.6)
            .reduce(1.0, (a, b) -> a * b);
    System.out.println("Product: " + product);

    // System.out.println(... math series:  i/2^i for i between 4 and 30
    double s = IntStream.rangeClosed(4,30)
                    .mapToDouble(i -> i / Math.pow(2,i))
                            .sum();
    System.out.println(s);

    // System.out.println(... concatenation of: {"Hello","World","!"}
    System.out.println(Stream.of("Hello","World","!").reduce("",(a,b) -> a + b));

    // System.out.println(... max of: sin^2*cos in [pi/4..2pi/3], 2002 samples
    double maxOf = sampling(Math.PI / 4, 2*Math.PI / 3,2002)
            .map(x-> Math.pow(Math.sin(x),2) * Math.cos(x))
            .max()
            .getAsDouble();
    System.out.println(maxOf);
  }

  // ------ Ex. 9 -----------
  public static void nipas(int n) {
    System.out.println(
      IntStream.range(0, n)
      .mapToObj(
        i -> IntStream.range(i, i+4)
             .mapToObj(j ->
                 " ".repeat(n+2-j) +
                 "*".repeat(1+2*j) )
             .collect(Collectors.joining("\n")))
      .collect(Collectors.joining("\n"))
    );
  }

  //-------------------------------------------------------
  public static void main(String[] args) {
    System.out.println("powers of two");
    printSomePowersOfTwo();

    System.out.println("mac address");
    System.out.println(macAddress(new int[]{78, 26, 253, 6, 240, 13}));

    System.out.println("divisors");
    System.out.println(Arrays.toString(divisors(496L)));

    System.out.println("sum of divisors");
    System.out.println(sumOfDivisors(496L));

    System.out.println("perfect numbers");
    System.out.println(Arrays.toString(perfectNumbers(10_000)));
  
    try {
      printMagicWord();
      printPalindromes();
    } catch(IOException e) {
      System.out.println("!! Problem when reading file... " + e.getMessage());
    }

    averages();
    averagesWithStreams();
    ex8();

     nipas(4); // read/analyze the code first!...
  }

}
