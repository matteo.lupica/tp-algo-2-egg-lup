package s02;

public class IntQueueChained {
  private static final int NIL = -1;
  private static int[] elements;  // Array to store the queue elements
  private static int[] nexts;     // Array to store the "next" pointers (indices)
  private static int firstFreeCell; // Pointer to the first available free cell
  private int front;  // Pointer to the front of the queue
  private int back;   // Pointer to the back of the queue

  //======================================================================
  // Constructor
  //======================================================================
  public IntQueueChained() {
    int initialCapacity = 10;
    elements = new int[initialCapacity];
    nexts = new int[initialCapacity];
    firstFreeCell = 0;
    front = NIL;
    back = NIL;
    for (int i = 0; i < initialCapacity - 1; i++) {
      nexts[i] = i + 1; // Each cell points to the next free cell
    }
    nexts[initialCapacity - 1] = NIL; // Last free cell points to NIL
  }

  //======================================================================
  // Memory Allocation and Deallocation Methods
  //======================================================================
  private static int allocate(int elt, int next) {
    if (firstFreeCell == NIL) {
      expandMemory();
    }
    int cell = firstFreeCell;
    firstFreeCell = nexts[cell]; // Update first free cell pointer
    elements[cell] = elt;        // Store the element in the allocated cell
    nexts[cell] = next;          // Set the next pointer
    return cell;
  }

  private static void deallocate(int i) {
    nexts[i] = firstFreeCell;
    firstFreeCell = i;
  }

  private static void expandMemory() {
    int newCapacity = nexts.length * 2;
    int[] tempNexts = new int[newCapacity];
    int[] tempElements = new int[newCapacity];
    System.arraycopy(nexts, 0, tempNexts, 0, nexts.length);
    System.arraycopy(elements, 0, tempElements, 0, elements.length);
    for (int i = nexts.length; i < newCapacity - 1; i++) {
      tempNexts[i] = i + 1;
    }
    tempNexts[newCapacity - 1] = NIL;
    firstFreeCell = nexts.length;
    nexts = tempNexts;
    elements = tempElements;
  }

  //======================================================================
  // Queue Operations
  //======================================================================
  public void enqueue(int elt) {
    int newCell = allocate(elt, NIL); // Allocate a new cell for the element
    if (back == NIL) {
      front = newCell; // Front and back both point to the new cell
      back = newCell;
    } else {
      nexts[back] = newCell; // Link the current back to the new cell
      back = newCell;        // Update the back pointer
    }
  }

  public boolean isEmpty() {
    return front == NIL;
  }

  public int consult() {
    if (isEmpty()) {
      throw new IllegalStateException("Queue is empty");
    }
    return elements[front];
  }
  public int dequeue() {
    if (isEmpty()) {
      throw new IllegalStateException("Queue is empty");
    }
    int elt = elements[front]; // Retrieve the front element
    int oldFront = front;      // Store the current front to deallocate later
    front = nexts[front];      // Move the front pointer to the next element
    if (isEmpty()) {
      back = NIL;
    }
    deallocate(oldFront);      // Deallocate the old front cell
    return elt;
  }
}
