package s12.ex06;

public final class ReversableLinkedList<E> implements IList<E> {

   static class Node<E> {
    E data;
    Node<E> next;
    Node<E> prev;

    Node(E data, Node<E> next, Node<E> prev) {
      this.data = data;
      this.next = next;
      this.prev = prev;
    }
  }
  Node<E> root;
  Node<E> current;
  int size;


  public ReversableLinkedList() {
    root = null;
    current = null;
    size = 0;

  }


  
  @Override
  public int size() {
    return size;
  }
  
  @Override
  public IListItr<E> iterator() {
    return new ReversableLinkedListItr<>(this);
  }
  
  @Override public String toString() {
    String s = "[";
    IListItr<E> li = iterator();
    while(!li.isLast()) {
      s += " " + li.consultAfter();
      li.goToNext();
    }
    return s + " ]";
  }
  
}
