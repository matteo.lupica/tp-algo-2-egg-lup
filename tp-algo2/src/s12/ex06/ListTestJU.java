package s12.ex06;
import java.util.*;
import java.util.function.Supplier;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public final class ListTestJU {
  static Random r = new Random(612);
  static boolean displayOperationLetter = false;

  @Test
  public void testReversableLinkedList() {
    Supplier<IList<Integer>> iListCreator = () -> new ReversableLinkedList<Integer>();
    randomizedTest(iListCreator);
  }

  public void randomizedTest(Supplier<IList<Integer>> iListCreator) {
    int nOperations = 1000;
    int nRepetitions = 100;
    for(int i=0; i<nRepetitions; i++)
      test(nOperations, iListCreator);
  }
  
  void test(int n, Supplier<IList<Integer>> iListCreator) {
    IList<Integer> l = iListCreator.get(); 
    IListItr<Integer> li = l.iterator();
    ListWithArray<Integer> lwa = new ListWithArray<>();
    IListItr<Integer> la = lwa.iterator();

    int a,i;
    for (i=0; i<n; i++) {
      switch(r.nextInt(7)) {
      case 0: // insert
        myPrint("i");
        a = r.nextInt(100);
        li.insertAfter(a); la.insertAfter(a);
        break;
      case 1: // remove
        if (la.isLast()) break;
        myPrint("r");
        li.removeAfter(); la.removeAfter();
        break;
      case 2: // next
        if (la.isLast()) break;
        myPrint("n");
        li.goToNext(); la.goToNext();
        break;
      case 3: // prev
        if (la.isFirst()) break;
        myPrint("p");
        li.goToPrev(); la.goToPrev();
        break;
      case 4: // first
        myPrint("f");
        li.goToFirst(); la.goToFirst();
        break;
      case 5: // last
        myPrint("l");
        li.goToLast(); la.goToLast();
        break;
      case 6: // reverseBefore
        myPrint("R");
        li.reverseBefore(); la.reverseBefore();
        break;
      }      
      // System.out.println(" " + li);
      assertEquals(lwa.size(), l.size());
      assertEquals(la.isFirst(), li.isFirst());
      assertEquals(la.isLast(), li.isLast());
      if(!li.isLast())
        assertEquals(la.consultAfter(), li.consultAfter());
    }
  }
  
  void myPrint(String s) {
    if(!displayOperationLetter) return;
    System.out.print(s);
  }
  //============================================================
  static final class ListWithArray<E> implements IList<E> {
    private ArrayList<E> v = new ArrayList<>();
    public int size() { 
      return v.size();
    }
    public IListItr<E> iterator() {
      return new ListWithArrayItr<E>(this);
    }
  }
  static final class ListWithArrayItr<E> implements IListItr<E> {
    private ListWithArray<E> lwa;
    int pos = 0;

    ListWithArrayItr(ListWithArray<E> lwa) {
      this.lwa = lwa;
    }
    public void  insertAfter(E e) { lwa.v.add(pos, e);        }
    public void  removeAfter()    { lwa.v.remove(pos);        } 
    public E    consultAfter()    { return lwa.v.get(pos);    }
    public void goToNext()        { pos++;                    }
    public void goToPrev()        { pos--;                    }
    public void goToFirst()       { pos=0;                    }
    public void goToLast()        { pos = lwa.size();         }
    public boolean isFirst()      { return pos == 0;          }
    public boolean isLast()       { return pos == lwa.size(); }
    public void reverseBefore()  {
      ArrayList<E> z = new ArrayList<>();
      for(int i=pos-1; i>=0; i--) z.add(lwa.v.get(i));
      for(int i=pos; i<lwa.v.size(); i++) z.add(lwa.v.get(i));
      this.lwa.v = z;
    }
  }

}
