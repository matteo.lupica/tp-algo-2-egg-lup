package s12.ex06;

// Similar to the specification we used in Algo1.
// Every operation in O(1) worst-case CPU.
public final class ReversableLinkedListItr<E> implements IListItr<E> {
  private ReversableLinkedList<E> list; //
  private ReversableLinkedList.Node<E> current;


  ReversableLinkedListItr(ReversableLinkedList<E> list) {
    this.list = list;
    this.current = null;
  }

  @Override
  public void insertAfter(E e) {
    ReversableLinkedList.Node<E> newNode = new ReversableLinkedList.Node<>(e, null, null);
    if (list.root == null) { // Empty list
      list.root = newNode;
      list.current = newNode;
    } else if (current == null) { // Insert at the beginning
      newNode.next = list.root;
      list.root.prev = newNode;
      list.root = newNode;
    } else { // Insert after the current node
      newNode.next = current.next;
      newNode.prev = current;
      if (current.next != null) {
        current.next.prev = newNode;
      }
      current.next = newNode;
    }
    list.size++;
  }

  @Override
  public void removeAfter() {
    if(isLast()) throw new IllegalStateException("can't removeAfter when isLast");
    ReversableLinkedList.Node<E> toRemove = current.next;
    if (toRemove == null) return;

    if (toRemove.next != null) {
      toRemove.next.prev = current;
    }
    current.next = toRemove.next;
    toRemove.next = null;
    toRemove.prev = null;
    list.size--;
  }

  @Override
  public E consultAfter() {
    if (isLast()) throw new IllegalStateException("Can't consultAfter when isLast");
    return current == null ? list.root.data : current.next.data;
  }

  @Override
  public void goToNext() {
    if (isLast()) throw new IllegalStateException("Can't goToNext when isLast");
    current = (current == null) ? list.root : current.next;
  }

  @Override
  public void goToPrev() {
    if (isFirst()) throw new IllegalStateException("Can't goToPrev when isFirst");
    current = (current == null) ? null : current.prev;
  }

  @Override
  public void goToFirst() {
    current = null;
  }

  @Override
  public void goToLast() {
    current = list.root;
    if (current != null) {
      while (current.next != null) {
        current = current.next;
      }
    }
  }

  @Override
  public boolean isFirst() {
    return current == null;
  }

  @Override
  public boolean isLast()  {
    return current == null ? list.root == null : current.next == null;
  }

  @Override
  public void reverseBefore() {
    if (current == null || current == list.root) return;

    ReversableLinkedList.Node<E> start = list.root;
    ReversableLinkedList.Node<E> prev = null;

    while (start != current.next) {
      ReversableLinkedList.Node<E> next = start.next;
      start.next = prev;
      start.prev = next;
      prev = start;
      start = next;
    }
    list.root.prev = current.next;
    list.root = current;
  }

}
