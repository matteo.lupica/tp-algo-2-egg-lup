package s12.ex06;

public interface IList<E> {
  int size();
  IListItr<E> iterator();
  default boolean isEmpty() { return size() == 0; }
}