package s12.ex14;

import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class Propagation {
  // Worst-case O(n) in CPU, O(1) in RAM
  public static void propagate2D(int[][] t, int e) {

   boolean firstRowContains = false;
   boolean firstColContains = false;


    //check if the first line contains the element
    for (int i = 0; i < t[0].length; i++) {
      if(t[0][i] == e){
        firstRowContains = true;
      }
    }

    //check if the first line contains the element
    for (int i = 0; i < t[0].length; i++) {
      if(t[i][0] == e){
        firstColContains = true;
      }
    }

    for (int i = 1; i < t.length; i++) {
      for (int j =1; j <t[i].length;j++){
        //check colum
        if( t[i][j] == e){
          t[i][0] = e;
          t[0][j] = e;
        }
       }
    }

    for (int i = 1; i < t.length; i++) {
      for (int j =1; j <t[i].length;j++) {
        if(t[i][0]==e || t[0][j]==e){
          t[i][j] = e;
        }
      }
      }
    if(firstColContains){
      for (int i = 0; i < t.length; i++) {
        t[i][0] = e;
      }
    }
    if(firstRowContains){
      for (int i = 0; i < t.length; i++) {
        t[0][i] = e;
      }
    }

  }

  // Worst-case O(n) in CPU, O(1) in RAM
  public static void propagate3D(int[][][] t, int e) {
    // TODO (s12.ex14b)
  }
  
  static int[][] deepCopy(int[][] t) {
    int n=t.length, m=t[0].length;
    int[][] res=new int[n][m];
    for(int i=0; i<n; i++)
      for(int j=0; j<m; j++)
        res[i][j] = t[i][j];
    return res;
  }
  
  static String asString(int[][] t) {
    return Arrays.stream(t)
        .map(z -> Arrays.toString(z))
        .collect(Collectors.joining("\n"))  + "\n";
  }

  static void demo() {
    int[][] t = {
        {1,3,2,4},
        {2,8,1,3},
        {2,5,3,2},
        {4,4,4,8}
    };
    int[][] r = {
        {1,8,2,8},
        {8,8,8,8},
        {2,8,3,8},
        {8,8,8,8}
    };

    System.out.println(asString(t));
    propagate2D(t, 8);
    System.out.println(asString(t));
    if(!asString(t).equals(asString(r)))
      System.out.println("Something's wrong...");
  }
  
  public static void main(String[] args) {
    demo();
  }
}
