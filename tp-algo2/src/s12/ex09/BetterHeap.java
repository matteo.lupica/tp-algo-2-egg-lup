
package s12.ex09;

// Every operation as efficient as removeMin() in a standard min-Heap
public class BetterHeap<E extends Comparable<E>> {
  // TODO

  public BetterHeap() { 
    // TODO
  }

  public void add(E e) {
    // TODO
  }

  public E removeMin() {
    return null; // TODO
  }

  public E removeMax() {
    return null; // TODO
  }

  public E min() {
    return null; // TODO
  }
  
  public E max() {
    return null; // TODO
  }

  public boolean isEmpty() {
    return false; // TODO
  }
}