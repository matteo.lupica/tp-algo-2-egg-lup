
package s12.ex18;

import java.util.Arrays;
import java.util.Random;

public class FindSuch2d {
  public record GridPosition(int i, int j) {}
  
  // PRE: t is an n x n square grid
  // Worst-case CPU : far better than O(n^2)
  public static GridPosition findSuchIJ(int[][] t) {
    return new GridPosition(0, 0); // TODO
  }
  
  static boolean isOk(int[][] t, GridPosition r) {
    int n = t.length, i = r.i(), j = r.j(), e = t[i][j];
    if(i > 0   && t[i-1][j] < e) return false;
    if(i+1 < n && t[i+1][j] < e) return false;
    if(j > 0   && t[i][j-1] < e) return false;
    if(j+1 < n && t[i][j+1] < e) return false;
    return true;
  }

  static int[][] deepCopyOf(int[][] t) {
    int[][] r = new int[t.length][];
    for(int i=0; i<t.length; i++)
      r[i] = Arrays.copyOf(t[i], t[i].length);
    return r;
  }
  
  static boolean gridEquals(int[][] t, int[][] u) {
    int n = t.length;
    if(u.length != n) return false;
    for(int i=0; i<n; i++) {
      int m = t[i].length;
      if(u[i].length != m) return false;
      for(int j=0; j<m; j++)
        if(t[i][j] != u[i][j]) return false;
    }
    return true;
  }
  
  static void testWith(int[][] t) {
    int[][] u = deepCopyOf(t);
    GridPosition r = findSuchIJ(t);
    if(!gridEquals(t, u)) throw new Error("bug - modifies the grid!");
    if(!isOk(t, r)) throw new Error("bug - wrong value!" + Arrays.deepToString(t) + " " + r );
  }

  static int[][] rndArray(int n, Random r) {
    int[][] t = new int[n][n];
    for (int i=0; i<n; i++) 
      for(int j=0; j<n; j++)
      t[i][j] = r.nextInt();
    return t;
  }


  static void smallTest() {
    Random rnd = new Random();
    int m = 10_000;
    long t0 = System.nanoTime();
    for(int n: new int[] {2, 3, 4, 7, 73, 1})
      for(int i=0; i<m; i++) {
        int[][] t = rndArray(n, rnd);
        testWith(t);
      }
    long dt = (System.nanoTime() - t0)/1000/1000;
    System.out.println("seems Ok... " + dt);
  }

  public static void main(String[] args) {
    smallTest();
  }
}
