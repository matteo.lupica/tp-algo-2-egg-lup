
package s12.ex11;

import java.awt.*;

public class LowestVertexInConvexPolygon {
  // PRE: polygon is convex polygon
  // POST: polygon[result] has minimal Y coordinate
  // Worst-case O(ln n) CPU

  //-2 : falling left and right
  public static int FF = -2;
  //-1 : rising left falling right
  public static int RF = -1;
  //0 : rising left and right
  public static int RR = 0;
  //1 : falling left rising right
  public static int FR = 1;


  public static int lowest(Point[] polygon) {
    int left = 0;
    int right = polygon.length -1;
    while (left <= right){
      int mid = getMidPoly(polygon,left,right);
      int edge = getEdge(mid, polygon);
      if(edge == RR){
        return mid;
      } else if(edge == RF){
        right = mid;
      }else {
        left = (mid +1);
      }
      if(left == right) break;
    }
    return left;

  }

  public static int getEdge(int index, Point[] polygon) {
    Point p = polygon[index]; // Current point
    Point pn = polygon[(index + 1) % polygon.length]; // Next point (wrap around)
    int prevIndex = index - 1 < 0 ? polygon.length - 1 : index - 1; // Previous point (wrap around)
    Point pp = polygon[prevIndex]; // Previous point

    // Determine the edge type based on the Y-coordinates
    int leftComparison  = comparePoints(p,pp);// Is the left edge rising?
    int rightComparison  = comparePoints(p,pn); // Is the right edge rising?

    if (leftComparison < 0 && rightComparison < 0) {
      return RR; // Rising left and right
    } else if (leftComparison < 0) {
      return RF; // Rising left, falling right
    } else if (rightComparison < 0) {
      return FR; // Falling left rising right
    } else { // !risingLeft && risingRight
      return FF; // Falling left and right
    }
  }


  public static boolean isFallingEdge(Point point1, Point point2) {
    return point2.y < point1.y; // True if the next point is lower
  }

  //returns the middle polygon of two points
  public static int getMidPoly (Point[] polygon, int left, int right){
    return  (left + right) / 2;
  }

  //compares 2 points, return -1 if p1< p2, 1 if not
  //compares Y cord, if both have same Y cord, returns the one with smaller x cord
  public static int comparePoints(Point p1, Point p2){
    if(p1.x < p2.x){
      return Integer.compare(p1.y, p2.y); // Compare Y coordinates
    } else{
      return Integer.compare(p1.x, p2.x);  // Compare X if Y coordinates are equal
    }
  }

  public static void main (String[] args){
    System.out.println("tests");
    Point[] polygon = {
            new Point(0, 1),
            new Point(3,2),
            new Point(2, -3),
            new Point(-1,-2)
    };

    Point[] polygon2 = {
            new Point(-5, 0),
            new Point(-3, 2),
            new Point(0, 3),
            new Point(2, 2),
            new Point(3, 0),
            new Point(-4, -2)
    };

    Point[] polygon3 = {
            new Point(-5, -1), // Point A (Lowest)
            new Point(2, 2),  // Point B
            new Point(0, -1),
    };

    System.out.println("polygon 1");
    for(Point p : polygon){
      System.out.print("["+p.x+";"+p.y+"],");
    }
    System.out.println();
    int lowestIndex = LowestVertexInConvexPolygon.lowest(polygon);
    System.out.println("Index of the lowest point: " + lowestIndex);
    System.out.println("Lowest point coordinates: (" + polygon[lowestIndex].x + ", " + polygon[lowestIndex].y + ")");


    System.out.println("polygon 2");
    for(Point p : polygon2){
      System.out.print("["+p.x+";"+p.y+"],");
    }
    System.out.println();
    int lowestIndex2 = LowestVertexInConvexPolygon.lowest(polygon2);
    System.out.println("Index of the lowest point: " + lowestIndex2);
    System.out.println("Lowest point coordinates: (" + polygon2[lowestIndex2].x + ", " + polygon2[lowestIndex2].y + ")");

    System.out.println("polygon 3");
    for(Point p : polygon3){
      System.out.print("["+p.x+";"+p.y+"],");
    }
    int lowestIndex3 = LowestVertexInConvexPolygon.lowest(polygon3);
    System.out.println("Index of the lowest point: " + lowestIndex3);
    System.out.println("Lowest point coordinates: (" + polygon3[lowestIndex3].x + ", " + polygon3[lowestIndex3].y + ")");
  }
}
