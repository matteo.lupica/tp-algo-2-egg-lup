
package s12.ex04;

import java.lang.reflect.Array;
import java.util.Arrays;

public class FindMindAndMax {
  public record MinAndMax(long min, long max) {}
  //======================================
  // Minimize the number of comparisons between elements (i.e. calls to
  // compareTo() if we adapt for an array of Comparable) :
  // - worst-case: no more than 1.7n comparisons
  // - best-case:  no more than 1.1n comparisons 
  // Note that finding the min needs (n-1) comparisons, thus the naive 
  // algorithm involves 2(n-1) comparisons)
  // PRE: t not empty
  public static MinAndMax minAndMax(long[] t) {

    int nbCompares = 0;

    nbCompares ++;
    //if length is 1, only elt is both min and max
    if(t.length == 1)
      return new MinAndMax(t[0],t[0]);


    long max = t[0]; // max in t
    long min = t[0]; // min in t
    long localMin; // local min between 2 elts
    long localMax; // local max between 2 elts
    boolean ascending = true;
    int i= 1;

    //best case is a sorted array
    while (i < t.length && t[i] >= t[i -1]){
      max = t[i];
      i++;
      nbCompares++;
    }

    nbCompares++;
    // If we traversed the whole array, it was fully sorted in ascending order.
    if (i == t.length) {
      System.out.println("n : " + t.length + " Number of comparisons: " + nbCompares);
      return new MinAndMax(min, max);
    }

    for (; i < t.length-1; i=i+2) {
      nbCompares++;
      if(t[i-1] < t[i]){
        localMin = t[i];
        localMax = t[i-1];
      }else{
        localMin =t[i-1];
        localMax = t[i];
      }

      if(localMin < min)
        min = localMin;
      nbCompares++;

      if(localMax > max)
        max = localMax;
      nbCompares++;
    }

    // If the array has an odd number of elements, process the last element.
    nbCompares ++;
    if (i == t.length - 1) {
      long lastElement = t[i];
      nbCompares ++;
      if (lastElement < min) {
        min = lastElement;
      }
      nbCompares ++;
      if (lastElement > max) {
        max = lastElement;
      }
    }

    System.out.println("n : " + t.length + " Number of comparisons: " + nbCompares);

    return new MinAndMax(min,max);
  }

  public static void main(String[] args) {
    long[] array = {1, 2, 3, 4, 5, 1, 6}; // Partially ascending
    MinAndMax result = minAndMax(array);
    System.out.println(Arrays.toString(array));
    System.out.println("Min: " + result.min() + ", Max: " + result.max());

    long[] array2 = {42, 7, 42, -19, 7, 56};
    result = minAndMax(array2);
    System.out.println(Arrays.toString(array2));
    System.out.println("Min: " + result.min() + ", Max: " + result.max());

    long[] fullySortedArray = {1, 2, 3, 4, 5}; // Fully sorted in ascending order
    System.out.println(Arrays.toString(fullySortedArray));
    result = minAndMax(fullySortedArray);
    System.out.println("Min: " + result.min() + ", Max: " + result.max());
  }


}
