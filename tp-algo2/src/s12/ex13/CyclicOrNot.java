
package s12.ex13;

import java.util.Random;

public class CyclicOrNot {

    private static final LNode CHECK_NODE = new LNode(null);

    public static class LNode {
        LNode next;

        LNode(LNode n) {
            next = n;
        }
    }
    // ===============================

    // O(n) in CPU, but strict O(1) in RAM
    public static boolean hasLoop(LNode head) {
        LNode initialHead = head;
        LNode nextHead = null;
        LNode prevHead = null;
        boolean res = false;
        while (head != null) {
            nextHead = head.next;
            if (head.next.equals(initialHead)) {
                res = true;
                break;
            }
            head.next = prevHead;
            prevHead = head;
            head = nextHead;
        }

        //if loop has been found
        if(res){

        }else{
            head = prevHead;
            prevHead = head.next;
            nextHead = null;
            ;
            //revert to original list
            while(head != null){
                prevHead = head.next;
                head.next = nextHead;
                nextHead = head;
                head = prevHead;
            }

        }


        return res;
    }

    public static boolean hasLoopRec(LNode head) {
        if (head == null) {
            return false;
        }

        if (head == CHECK_NODE) {
            return true;
        }

        LNode nextHead = head.next;
        head.next = CHECK_NODE;

        return hasLoop(nextHead);
    }

    static LNode makeList(int n, boolean hasLoop, Random rnd) {
        LNode last = new LNode(null);
        LNode first = last;
        for (int i = 0; i < n; i++)
            first = new LNode(first);
        if (hasLoop) {
            int x = rnd.nextInt(n);
            LNode anchor = first;
            for (int i = 0; i < x; i++)
                anchor = anchor.next;
            last.next = anchor;
        }
        return first;
    }

    public static void demo() {
        Random rnd = new Random();
        LNode a = makeList(20, false, rnd);
        LNode b = makeList(20, true, rnd);
        System.out.println(hasLoop(a));
        System.out.println(hasLoop(b));
    }

    public static void main(String[] args) {
        demo();
    }

}
