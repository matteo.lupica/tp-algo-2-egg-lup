
package s12.ex03;

import s12.ex01.Stack;

// Each operation in O(1) CPU worst-case
public class BetterStack {
  // TODO
  Stack<Float> prevMins;
  Stack<Float> prevMaxs;
  Stack<Float> buff;

  float min;
  float max;


  public BetterStack() {
    prevMins = new Stack<>();
    prevMaxs = new Stack<>();
    buff = new Stack<>();
    min = Float.POSITIVE_INFINITY;
    max = Float.NEGATIVE_INFINITY;
  }

  public void push(float e) {
    if(isEmpty()){

      buff.push(e);
      max = e;
      min = e;
    }else{
      if(e <= min){
        prevMins.push(min);
        min =e;
      }
      if(e >= max){
        prevMaxs.push(max);
        max = e;
      }
      buff.push(e);
    }
  }

  public float pop() {
      float r = buff.pop();

      if(max==r && !prevMaxs.isEmpty()){
        max = prevMaxs.pop();
      }

      if(min == r  && !prevMins.isEmpty()){
        min = prevMins.pop();
      }



      return r;
  }
  public float consultMin() {
   return  min;
  }

  public float consultMax() {
    return max;
  }
  public boolean isEmpty() {
    return buff.isEmpty();
  }



    public static void main(String[] args) {
      // Create a new BetterStack instance
      BetterStack bs = new BetterStack();

      // Test pushing elements onto the stack
      System.out.println("Pushing 10, 20, 5, 30 onto the stack:");
      bs.push(10);
      System.out.println("Min: " + bs.consultMin() + ", Max: " + bs.consultMax());
      bs.push(20);
      System.out.println("Min: " + bs.consultMin() + ", Max: " + bs.consultMax());
      bs.push(5);
      System.out.println("Min: " + bs.consultMin() + ", Max: " + bs.consultMax());
      bs.push(30);
      System.out.println("Min: " + bs.consultMin() + ", Max: " + bs.consultMax());

      // Test popping elements and checking the min/max after each pop
      System.out.println("\nPopping elements:");
      System.out.println("Popped: " + bs.pop());
      System.out.println("Min: " + bs.consultMin() + ", Max: " + bs.consultMax());
      System.out.println("Popped: " + bs.pop());
      System.out.println("Min: " + bs.consultMin() + ", Max: " + bs.consultMax());
      System.out.println("Popped: " + bs.pop());
      System.out.println("Min: " + bs.consultMin() + ", Max: " + bs.consultMax());
      System.out.println("Popped: " + bs.pop());
      System.out.println("Min: " + bs.consultMin() + ", Max: " + bs.consultMax());
    }


}
