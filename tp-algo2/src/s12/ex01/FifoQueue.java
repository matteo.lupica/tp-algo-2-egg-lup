
package s12.ex01;

// Implement this class, with amortized O(1) for each operation; 
// The only collection available is Stack (no array, no chaining nodes etc.)
public class FifoQueue<E> {
  private Stack<E> stackIn = new Stack<>();
  private Stack<E> stackOut = new Stack<>();

  public FifoQueue() {
    stackIn = new Stack<E>(); // stack where enqueued elements are added
    stackOut = new Stack<E>(); // stack that contains elements in reversed order
  }

  public void enqueue(E elt) {
    stackIn.push(elt);
  }

  public E dequeue() {
    fillOutStack();
    return stackOut.pop();
  }

  public E consultOldest() {
    fillOutStack();
    return stackOut.top();
  }

  //puts elements from instack to outstack
  //worst case complexity is O(n) but a amortized complexity of O(1) is guarantied
  private void fillOutStack() {
    if (stackOut.isEmpty()) { //while there are still older elements in the outqueue, there is no need for change
      //pop all elements in inqueue and push them onto outstack
      while (!stackIn.isEmpty()) {
        stackOut.push(stackIn.pop());
      }
    }
  }

  public boolean isEmpty() {
    // if stack is empty so is queue
    return stackOut.isEmpty() && stackIn.isEmpty();
  }

  //main class to test
  public static void main(String[] args) {
    FifoQueue<Integer> queue = new FifoQueue<>();

    // Enqueue elements
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    System.out.println("Enqueued: 1, 2, 3");

    // Consult oldest element
    System.out.println("Oldest element: " + queue.consultOldest()); // Should print 1

    // Dequeue an element
    System.out.println("Dequeued: " + queue.dequeue()); // Should print 1
    System.out.println("Is the queue empty? " + queue.isEmpty()); //should print false

    // Consult oldest element again
    System.out.println("Oldest element: " + queue.consultOldest()); // Should print 2

    // Dequeue remaining elements
    System.out.println("Dequeued: " + queue.dequeue()); // Should print 2
    System.out.println("Dequeued: " + queue.dequeue()); // Should print 3

    // Check if the queue is empty
    System.out.println("Is the queue empty? " + queue.isEmpty()); // Should print true
  }
}