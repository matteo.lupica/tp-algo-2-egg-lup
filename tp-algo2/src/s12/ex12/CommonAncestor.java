
package s12.ex12;

public class CommonAncestor {
  static class BTNode {
    Object elt;
    BTNode left, right, parent;

    public BTNode(Object e, BTNode l, BTNode r, BTNode p) {
      elt = e; left = l; right = r; parent = p;
    }
  }
  //===============================================
  // O(height) in CPU, but strict O(1) in RAM
  public static BTNode commonAncestor(BTNode a, BTNode b) {

    if(a.parent.equals(b.parent)){
      return a.parent;
    }
   return null;
  }
}
