
package s12.ex07;

public class MissingIntInFile {
  // s12.ex7
  public static int oneOfTheMissingInts(String filename) {
    return 0;  // TODO
  }
  
  // s12.ex7b: PRE: the file has all numbers between 0-n except a single one
  public static int singleMissingInt(String filename, int n) {
    return 0;  // TODO
  }
 
}
