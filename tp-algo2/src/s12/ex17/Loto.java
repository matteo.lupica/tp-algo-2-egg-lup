
package s12.ex17;

import java.util.HashMap;
import java.util.Random;

public class Loto {
  private final Random rnd;
  // element buffer
  long[] elts;
  //separate values used one more time than others
  int offset1Time;

  // PRE: no duplicates, length > 0
  public Loto(long[] elements, Random r) {
    this.elts = elements;
    this.rnd = r;
    offset1Time = 0;
   // offset2Time = -1;

  }

  // Returns one of the array element
  // If #i counts "how often element n° i has been chosen", 
  // for any i,j we must ensure that |#i - #j| <= 1
  // Worst-case O(1) CPU
  public long next() {

    if(offset1Time == elts.length){
      offset1Time = 0;
    }

    int rndIndex = rnd.nextInt(offset1Time, elts.length);
    long next = elts[rndIndex];

    // Swap the selected element with the one at the offset position
    swap(offset1Time, rndIndex);
    offset1Time++;


    System.out.println("val: " + next + " rndIndex: " + rndIndex + " offset1: " + offset1Time);

    return next;
  }

  // method to swap to values
  private void swap(int x, int y){
    long temp = elts[x];
    elts[x] = elts[y];
    elts[y] = temp;
  }

  public static void main( String[] args){

    //long elt, nb of occurrences
    HashMap<Long,Integer> map = new HashMap<>();
    long[] arr = {1,2,3,4};
    Random rnd = new Random();

    Loto loto = new Loto(arr,rnd);

    for (int i = 0; i < 20; i++) {
      long elt =       loto.next();
      if(map.containsKey(elt)){
        map.put(elt,map.get(elt)+1);
      }else {
        map.put(elt,1);
      }
    }
    for (long key: map.keySet()){
      System.out.println("val : "+ key + "nb : " + map.get(key));
    }
  }
}
