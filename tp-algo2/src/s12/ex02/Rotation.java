
package s12.ex02;

import java.util.Arrays;

public class Rotation {
  // must be O(n) in CPU, and O(1) in RAM
  public static void rotate(int[] t, int i) {
    //nno point rotating an empty array or one if only 1 elt
    if(t.length < 2) return;

    int nbSwaps = 0;
    int start = 0;

    int currIndex = 0;
    int currElt = t[currIndex];
    int targetIndex = -1;
    int temp;

    while(nbSwaps <  t.length) {
      currIndex = start;
      currElt = t[currIndex];
      do {
        targetIndex = getTargetIndex( t.length, i, currIndex);
        temp = t[targetIndex];
        t[targetIndex] = currElt;

        currElt = temp;
        currIndex = targetIndex;
        nbSwaps++;
      }while (currIndex != start);
      start++; //move to next index
    }

  }

  private  static int getTargetIndex(int size, int rotationValue, int index){
    int target = index - rotationValue;
    if (target < 0){
       target = (size + target);
    }
    return target;
  }
  
  public static void main(String[] args) {
    int[] t = {0,1,2,3,4};
    System.out.print(Arrays.toString(t)+" --> ");
    rotate(t, 2);
    System.out.println(Arrays.toString(t)); //  --> [2, 3, 4, 0, 1]
    t = new int[]{0,1,2,3};
    System.out.print(Arrays.toString(t)+" --> ");
    rotate(t, 2); 
    System.out.println(Arrays.toString(t)); //  --> [2, 3, 0, 1]
  }
}
