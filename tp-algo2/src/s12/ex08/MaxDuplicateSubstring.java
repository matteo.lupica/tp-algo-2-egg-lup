
package s12.ex08;

import java.util.HashMap;

public class MaxDuplicateSubstring {
  // Design an approach significantly better than the naive algorithm below
  // (and try to discuss the respective worst-case O(…) complexities)
  public static String maxDuplicate(String s) {
    int maxLength = 0;
    String maxString = "";
    String curr = "";

    HashMap<String,Integer> map = new HashMap<>();
    for (int i = 0; i < s.length(); i++) {

    }
    return null;  // TODO
  }
  
  public static String maxDuplicateNaive(String s) {
    String res = "";
    for(int i=0; i<s.length(); i++) {
      for(int j=i+1; j<s.length(); j++) {
        for(int len=res.length()+1; len<=s.length()-j; len++) {
          String a = s.substring(i, i+len);
          String b = s.substring(j, j+len);
          if(!a.equals(b)) break;
          res = a;
        }
      }
    }
    return res;
  }
  
  public static void main(String[] args) {
    String s = "abc defc defc defgh i";
    System.out.println(maxDuplicateNaive(s));
    System.out.println(maxDuplicate(s));
  }
}
