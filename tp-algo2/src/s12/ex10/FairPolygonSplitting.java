
package s12.ex10;

import java.awt.Point;

public class FairPolygonSplitting {
  public record SplitSolution(int from, int to, double cost) {
    // one part is made of corners from, from+1, ..., to
    // cost is abs(perimA - perimB), we want to minimize that
    
    @Override public String toString() {
      return String.format("[%d,%d : %f]", from, to, cost);
    }
  }
  //=============================================
  // Worst-case CPU O(n)
  public static SplitSolution fairSplit(Point[] polygon) {
    double[] perimeters = new double[polygon.length]; // store the perims, total perims will be the sum of all

    double totalPerimeter = 0;
    for (int i = 0; i < polygon.length; i++) {
      // add distance between current point and next point to total perim
      totalPerimeter += distance(polygon[i], polygon[(i + 1) % polygon.length]);
      // perim to index i will be the sum of all perims up to now
      perimeters[i] = totalPerimeter;
    }

    //start and end of current segment
    int start = 0, end = 1;
    SplitSolution bestSolution = new SplitSolution(0, 0, Double.MAX_VALUE);

    while(start < polygon.length) {
      //perim of current segment, cost to start is 0
      double segmentPerimeter = perimeters[end % polygon.length] - (start > 0 ? perimeters[start - 1] : 0);
      //perim of rest
      double otherPerimeter = totalPerimeter - segmentPerimeter;
      // current cost
      double cost = Math.abs(segmentPerimeter - otherPerimeter);

      //if cost is better, best solution is updated
      if (cost < bestSolution.cost) {
        bestSolution = new SplitSolution(start, end % polygon.length, cost);
      }

      if (segmentPerimeter < (totalPerimeter / 2) && end < polygon.length) {
        end++; // segment not long enough
      } else {
        start++; // segment to large
        if (start > end) {
          end = start + 1; // keep end after start
        }
      }
    }

    return bestSolution;
  }

  private static double distance(Point p1, Point p2) {
    return Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));
  }

    public static void main(String[] args) {
      // Test cases for the fair polygon splitting
      Point[] p1 = {
              new Point(0, 0),
              new Point(4, 0),
              new Point(2, 3)
      };

      Point[] p2 = {
              new Point(0, 0),
              new Point(4, 0),
              new Point(4, 4),
              new Point(0, 4)
      };

      Point[] p3 = {
              new Point(0, 0),
              new Point(2, 3),
              new Point(4, 0),
              new Point(3, -3),
              new Point(1, -3)
      };

      // Run tests
      testPolygon("p1", p1);
      testPolygon("p2", p2);
      testPolygon("p3", p3);
    }

    private static void testPolygon(String name, Point[] polygon) {
      System.out.printf("Testing %s:\n", name);
      FairPolygonSplitting.SplitSolution solution = FairPolygonSplitting.fairSplit(polygon);
      System.out.printf("Best split points: %s\n\n", solution);
    }
  }


