package s12.ex05;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

// RAM O(n)
public class AnagramDico {


  private HashMap<String, HashSet<String>> dict;

  // worst-case CPU O(n ln n)
  public AnagramDico(String[] words) {
    dict = new HashMap<>();
    for (String word : words) {

      //convert the word to a sorted char Array
      char[] ca = word.toCharArray();
      Arrays.sort(ca);
      //rebuild a new String with the sorted array
      String key = new String(ca);

      //get key from set
      HashSet<String> set = dict.get(key);
      //if the key is found, add the word to the set
      if(set!=null){
        set.add(word);
      }else{
        //if it is not found, crate a new set
        set = new HashSet<String>();
        set.add(word);
        //put new key value in the dict
        dict.put(key,set);
      }
    }
  }
  
  // worst-case CPU O(K + ln n);  K=res.size(), output-sensitive
  public Set<String> anagramsOf(String s) {
    Set<String> res = new HashSet<>();

    //get the key, sorted String
    char[] ca = s.toCharArray();
    Arrays.sort(ca);
    String key = new String(ca);

    //get key from the dict
    Set<String> targetSet = dict.get(key);
    //if its found travrse words
    if(targetSet!=null){
      //a word is not an anagram of itself, so the set must be traversed
      for (String word : targetSet) {
        // add the word to res if it is not the word given in params
          if(!word.equals(s)){
            res.add(word);
          }
      }
    }
    return res;
  }

  // ----------------------------------------------------------------------
  static void demo() {
    String[] t = {"spot","zut","pots", "stop", "hello"};
    AnagramDico d = new AnagramDico(t);
    d = new AnagramDico(t);
    var res = d.anagramsOf("tops");
    System.out.println(res); // [stop, spot, pots]
  }
  
  public static void main(String[] args) {
    demo();
  }
}
