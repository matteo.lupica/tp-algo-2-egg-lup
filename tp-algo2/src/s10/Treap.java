package s10;
import java.util.Random;
//--------------------------------------------------
public class Treap<E extends Comparable<E>>  {
  //============================================================
  static class TreapElt<E extends Comparable<E>> implements Comparable<TreapElt<E>> {
    static final Random rnd = new Random(); 
    // -----------------------
    private final E elt;
    private final int pty;
    // -----------------------
    public TreapElt(E e) {
      elt = e; 
      pty = rnd.nextInt();
    }

    public int pty() {
      return pty;
    }

    public E elt() {
      return elt;
    }

    public int compareTo(TreapElt<E> o) {
      return elt.compareTo(o.elt);
    }

    @Override public boolean equals(Object o) {
      if(o == null) return false;
      if (this.getClass() != o.getClass()) return false;
      if (elt == null) return false;
      return elt.equals(((TreapElt<?>)o).elt);
    }

    @Override public String toString() {
      return "" + elt + "#" + pty;
    }

    @Override public int hashCode() {
      return elt.hashCode();
    }
  }


  //============================================================
  private final BST<TreapElt<E>> bst;
  // --------------------------
  public Treap() {
    bst = new BST<TreapElt<E>>();
  }

  public void add(E e) {
    TreapElt<E> newTE=  new TreapElt<>(e);
    bst.add(newTE);
    percolateUp(bst.locate(newTE));
  }

  public void remove(E e) {
    TreapElt<E> newTE=  new TreapElt<>(e);

    BTreeItr <TreapElt<E>> it = bst.locate(newTE);
    // return if e is not in tree

    if(it.isBottom()) return;
    siftDownAndCut(it);
    bst.crtSize--;
  }


  public boolean contains(E e) {
    return bst.contains(new TreapElt<E>(e));
  }

  public int size() {
    return bst.size();
  }

  public E minElt() {
    return bst.minElt().elt;
  }

  public E maxElt() {
    return bst.maxElt().elt;
  }
  
  public String toString() {
    return bst.toString();
  }

  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  private void siftDownAndCut(BTreeItr<TreapElt<E>> ti) {
    while (!ti.isLeafNode()){
      if(goLeft(ti.left(),ti.right())){
        //stop if bottom has lower prio
        ti.rotateRight();
        ti = ti.right();

      }else{
        //stop if bottom has lower prio
       ti.rotateLeft();
       ti = ti.left();
      }
    }

    assert(ti.isLeafNode());
    ti.cut();
  }

  //PRE: either left or right is not bottom
  //true go left, false go right
  private boolean goLeft(BTreeItr<TreapElt<E>> left,BTreeItr<TreapElt<E>> right  ){
    if(left.isBottom()){
      //if left is bottom, go right
      return false;
    } else if (right.isBottom()) {
      //if right is bottom go left
      return true;
    }
    // return min between left and right
     return left.consult().elt().compareTo(right.consult().elt()) < 0;
  }

  private void percolateUp(BTreeItr<TreapElt<E>> ti) {
    while((!ti.isRoot()) && isLess(ti, ti.up())) {
      if (ti.isLeftArc()) {
        ti=ti.up();
        ti.rotateRight();
      }else{
        ti=ti.up();
        ti.rotateLeft();
      }
    }
  }

  private boolean isLess(BTreeItr<TreapElt<E>> a, BTreeItr<TreapElt<E>> b) {
    TreapElt<E> ca = a.consult();
    TreapElt<E> cb = b.consult();
    return ca.pty() < cb.pty();
  }
}
