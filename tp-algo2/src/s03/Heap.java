package s03;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Heap<E extends Comparable<E>> {
  private final ArrayList<HeapNode> buffer;
  private long elementId;  // To track the insertion order

  public Heap() {
    buffer = new ArrayList<>();
    elementId = 0;
  }

  private class HeapNode {
    E element;
    long elementId;

    HeapNode(E element, long elementId) {
      this.element = element;
      this.elementId = elementId;
    }
  }

  public void add(E e) {
    HeapNode node = new HeapNode(e, elementId++);
    buffer.add(node);
    percolateUp(buffer.size() - 1);
  }

  public E removeMin() {
    if (isEmpty()) {
      return null;
    }
    E minElement = min();
    int last = buffer.size() - 1;
    buffer.set(0, buffer.get(last));
    buffer.remove(last);
    siftDown(0);
    return minElement;
  }

  public E min() {
    return buffer.get(0).element;
  }

  public boolean isEmpty() {
    return buffer.isEmpty();
  }

  public String toString() {
    StringBuilder res = new StringBuilder("Array: ");
    for (HeapNode node : buffer) {
      res.append(node.element).append(" ");
    }
    return res + "\n Tree: " + toReadableString(0);
  }

  private void siftDown(int i) {
    while (leftChild(i) < buffer.size()) {
      int smallerChild = leftChild(i);

      if (rightChild(i) < buffer.size() && compareNodes(rightChild(i), leftChild(i)) < 0) {
        smallerChild = rightChild(i);
      }

      if (compareNodes(smallerChild, i) < 0) {
        swap(i, smallerChild);
        i = smallerChild;
      } else {
        break;
      }
    }
  }

  private void percolateUp(int i) {
    while (i > 0 && compareNodes(i, parent(i)) < 0) {
      swap(i, parent(i));
      i = parent(i);
    }
  }

  private int compareNodes(int i, int j) {
    HeapNode node1 = buffer.get(i);
    HeapNode node2 = buffer.get(j);
    int cmp = node1.element.compareTo(node2.element);

    if (cmp == 0) {
      return Long.compare(node1.elementId, node2.elementId);  // Compare by elementId if priorities are equal
    }

    return cmp;
  }

  private int parent(int i) {
    return (i - 1) / 2;
  }

  private int leftChild(int i) {
    return 2 * i + 1;
  }

  private int rightChild(int i) {
    return 2 * i + 2;
  }

  private void swap(int i, int j) {
    HeapNode temp = buffer.get(i);
    buffer.set(i, buffer.get(j));
    buffer.set(j, temp);
  }

  public String toReadableString(int r) {
    List<StringBuilder> b = subtreeBlock(r);
    StringBuilder res = new StringBuilder("\n");
    for (StringBuilder s : b) res.append(s).append('\n');
    return res.toString();
  }

  private List<StringBuilder> subtreeBlock(int n) {
    List<StringBuilder> ls, rs, res = new ArrayList<>();
    if (n >= buffer.size()) {
      res.add(new StringBuilder(""));
      return res;
    }
    String eltStr = String.valueOf(buffer.get(n).element);
    ls = subtreeBlock(leftChild(n));
    rs = subtreeBlock(rightChild(n));
    int lw = ls.get(0).length();
    int rw = rs.get(0).length();
    if (lw + rw == 0) {
      res.add(new StringBuilder(eltStr));
      return res;
    }
    replaceExtremeSpaces(ls.get(0), SPACE, LMARK);
    replaceExtremeSpaces(rs.get(0), RMARK, SPACE);
    String sep = FIRST_SEP;
    for (int i = 0; i < ls.size() || i < rs.size(); i++) {
      StringBuilder l = (i < ls.size()) ? ls.get(i) : blockOf(lw, SPACE);
      StringBuilder r = (i < rs.size()) ? rs.get(i) : blockOf(rw, SPACE);
      res.add(l.append(sep).append(r));
      sep = OTHER_SEP;
    }
    StringBuilder first = blockOf(lw + 1, SPACE).append(eltStr);
    int pad = (lw + rw + sep.length()) - first.length();
    if (pad >= 0) {
      first.append(blockOf(pad, SPACE));
    } else {
      StringBuilder suffix = blockOf(-pad, SPACE);
      for (StringBuilder z : res)
        z.append(suffix);
    }
    res.add(0, first);
    return res;
  }

  private static void replaceExtremeSpaces(StringBuilder sb, char c, char d) {
    int n = sb.length();
    for (int i = 0; i < n && sb.charAt(i) == SPACE; i++) sb.setCharAt(i, c);
    for (int i = n - 1; i >= 0 && sb.charAt(i) == SPACE; i--) sb.setCharAt(i, d);
  }

  private static StringBuilder blockOf(int w, char c) {
    StringBuilder r = new StringBuilder("");
    while (w-- > 0) r.append(c);
    return r;
  }

  private static final char SPACE = '\u00A0';
  private static final char LMARK = '´';
  private static final char RMARK = '`';
  private static final String FIRST_SEP = "" + LMARK + SPACE + RMARK;
  private static final String OTHER_SEP = "" + SPACE + SPACE + SPACE;

  public static void main(String[] args) {
    int n = 10;
    if (args.length > 0) n = Integer.parseInt(args[0]);
    Random r = new Random();
    Heap<Integer> h = new Heap<>();
    Integer e;
    for (int i = 0; i < n; i++) {
      String log = "";
      if (r.nextInt(10) < 4 && !h.isEmpty()) {
        e = h.removeMin();
        log += "\n--- remove " + e + "\n" + h;
      } else {
        e = r.nextInt(n);
        h.add(e);
        log += "\n--- add    " + e + "\n" + h;
      }
      if (i < 20) System.out.println(log);
    }
  }
}
