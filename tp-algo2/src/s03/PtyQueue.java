package s03;

/** For this priority queue, we consider that a "small" value  
 *  of type P represents a stronger priority than a "big" one  */
public class PtyQueue<E, P extends Comparable<P>> {
  private final Heap<HeapElt> heap;

  public PtyQueue() {
    heap = new Heap<>();
  }

  public boolean isEmpty() {
    return heap.isEmpty();
  }

  /** Adds an element elt with priority pty */ 
  public void enqueue(E elt, P pty) {
    heap.add(new HeapElt(pty, elt));
  }
  
  /** Returns the element with the strongest priority. PRE: !isEmpty() */
  public E consult() {
    return heap.min().getTheElt();
  }

  /** Returns the priority of the element with the strongest priority.
   *  PRE: !isEmpty() */ 
  public P consultPty() {
    return heap.min().getThePty();
  }
  
  /** Removes and returns the element with the strongest priority.
   *  PRE: !isEmpty() */ 
  public E dequeue() {
    return heap.removeMin().getTheElt();
  }

  @Override public String toString() {
    return heap.toString(); 
  }
  //=============================================================
  class HeapElt implements Comparable<HeapElt> {
    private final P thePty;
    private final E theElt;
    
    public HeapElt(P thePty, E theElt) {
      this.thePty = thePty;
      this.theElt = theElt;
    }

    public E getTheElt() {
      return theElt;
    }

    public P getThePty() {
      return thePty;
    }

    @Override public int compareTo(HeapElt arg0) {
      return thePty.compareTo(arg0.thePty);
    }
  }
}

