package s09;

// ------------------------------------------------------------ 
public class StringSearching {
  // ------------------------------------------------------------ 
  static /*final*/ int HASHER = 301; // Maybe also try with 7 and 46237
  static /*final*/ int BASE   = 256; // Please also try with 257
  // ---------------------
  static int firstFootprint(String s, int len) {
    int n = s.charAt(0) % HASHER;

    for (int i = 1; i < len; i++) {
      n *= (BASE % HASHER);
      n += (s.charAt(i) % HASHER);
      n %= (HASHER);
    }

    return (n % HASHER);

  }

  static boolean compareHash(long x, long y){
    return (x / HASHER) == (y / HASHER);
  }
  // ---------------------
  // must absolutely be O(1)
  // coef is (BASE  power  P.LENGTH-1)  mod  HASHER
  static int nextFootprint(int previousFootprint, char dropChar, char newChar, int coef) {
    int h = previousFootprint;
    int dropVal = dropChar*coef % HASHER;
    h *= BASE % HASHER;
    h = previousFootprint - dropVal;
    h = BASE * (previousFootprint - dropVal) + newChar%HASHER;
    // TODO - A COMPLETER
    // h = previousFootprint
    // h = h - ...               // dropChar        (bien réfléchir !)
    // h = h * ...               // shift
    // h = h + ...               // newChar
    return h;
  }
  // ---------------------
  // Rabin-Karp algorithm
  public static int indexOf_rk(String t, String p) {

    int pHash = firstFootprint(p,p.length());





return 0;
  }
}
